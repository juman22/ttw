--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE typeormtest;




--
-- Drop roles
--

DROP ROLE typeormtest;


--
-- Roles
--

CREATE ROLE typeormtest;
ALTER ROLE typeormtest WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:Xf6i89Yy87Up4Mil8aODvw==$mfJzNjZQY0QZ9nIU7tcak86yL5V6VWuU2QhbGvLTRfI=:SwSW3H4oVU7WEi1qdqPnScT/JMcdM4pg+PJ/C9Q+PJs=';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: typeormtest
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO typeormtest;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: typeormtest
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: typeormtest
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: typeormtest
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: typeormtest
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO typeormtest;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: typeormtest
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- Database "typeormtest" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: typeormtest; Type: DATABASE; Schema: -; Owner: typeormtest
--

CREATE DATABASE typeormtest WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE typeormtest OWNER TO typeormtest;

\connect typeormtest

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: character; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public."character" (
    id integer NOT NULL,
    name character varying NOT NULL,
    hp integer NOT NULL,
    "maxHp" integer NOT NULL,
    "endurantCharges" integer NOT NULL,
    "userId" integer,
    "settingId" integer,
    "raceId" integer,
    "clazzId" integer,
    notes text DEFAULT ''::text NOT NULL,
    toxicity integer DEFAULT 0 NOT NULL,
    hunger integer DEFAULT 0 NOT NULL
);


ALTER TABLE public."character" OWNER TO typeormtest;

--
-- Name: character_class_feat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_class_feat (
    id integer NOT NULL,
    "characterId" integer,
    "featId" integer
);


ALTER TABLE public.character_class_feat OWNER TO typeormtest;

--
-- Name: character_class_feat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_class_feat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_class_feat_id_seq OWNER TO typeormtest;

--
-- Name: character_class_feat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_class_feat_id_seq OWNED BY public.character_class_feat.id;


--
-- Name: character_effect; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_effect (
    id integer NOT NULL,
    "characterId" integer,
    "statusEffectId" integer
);


ALTER TABLE public.character_effect OWNER TO typeormtest;

--
-- Name: character_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_effect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_effect_id_seq OWNER TO typeormtest;

--
-- Name: character_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_effect_id_seq OWNED BY public.character_effect.id;


--
-- Name: character_equipment; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_equipment (
    id integer NOT NULL,
    "isEquipped" boolean NOT NULL,
    "characterId" integer,
    "equipmentId" integer
);


ALTER TABLE public.character_equipment OWNER TO typeormtest;

--
-- Name: character_equipment_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_equipment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_equipment_id_seq OWNER TO typeormtest;

--
-- Name: character_equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_equipment_id_seq OWNED BY public.character_equipment.id;


--
-- Name: character_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_id_seq OWNER TO typeormtest;

--
-- Name: character_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_id_seq OWNED BY public."character".id;


--
-- Name: character_item; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_item (
    id integer NOT NULL,
    quantity integer NOT NULL,
    "characterId" integer,
    "itemId" integer
);


ALTER TABLE public.character_item OWNER TO typeormtest;

--
-- Name: character_item_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_item_id_seq OWNER TO typeormtest;

--
-- Name: character_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_item_id_seq OWNED BY public.character_item.id;


--
-- Name: character_skill; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_skill (
    id integer NOT NULL,
    experience integer NOT NULL,
    "characterId" integer,
    "skillId" integer
);


ALTER TABLE public.character_skill OWNER TO typeormtest;

--
-- Name: character_skill_feat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_skill_feat (
    id integer NOT NULL,
    "characterId" integer,
    "featId" integer
);


ALTER TABLE public.character_skill_feat OWNER TO typeormtest;

--
-- Name: character_skill_feat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_skill_feat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_skill_feat_id_seq OWNER TO typeormtest;

--
-- Name: character_skill_feat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_skill_feat_id_seq OWNED BY public.character_skill_feat.id;


--
-- Name: character_skill_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_skill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_skill_id_seq OWNER TO typeormtest;

--
-- Name: character_skill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_skill_id_seq OWNED BY public.character_skill.id;


--
-- Name: character_stat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_stat (
    id integer NOT NULL,
    value integer NOT NULL,
    "characterId" integer,
    "statId" integer
);


ALTER TABLE public.character_stat OWNER TO typeormtest;

--
-- Name: character_stat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_stat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_stat_id_seq OWNER TO typeormtest;

--
-- Name: character_stat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_stat_id_seq OWNED BY public.character_stat.id;


--
-- Name: class; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.class (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer
);


ALTER TABLE public.class OWNER TO typeormtest;

--
-- Name: class_feat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.class_feat (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "clazzId" integer,
    description character varying(2000) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.class_feat OWNER TO typeormtest;

--
-- Name: class_feat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.class_feat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.class_feat_id_seq OWNER TO typeormtest;

--
-- Name: class_feat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.class_feat_id_seq OWNED BY public.class_feat.id;


--
-- Name: class_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.class_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.class_id_seq OWNER TO typeormtest;

--
-- Name: class_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.class_id_seq OWNED BY public.class.id;


--
-- Name: equipment; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.equipment (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "universalModifierId" integer,
    notes character varying(2000) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.equipment OWNER TO typeormtest;

--
-- Name: equipment_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.equipment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipment_id_seq OWNER TO typeormtest;

--
-- Name: equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.equipment_id_seq OWNED BY public.equipment.id;


--
-- Name: equipment_skill_modifiers_skill_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.equipment_skill_modifiers_skill_modifier (
    "equipmentId" integer NOT NULL,
    "skillModifierId" integer NOT NULL
);


ALTER TABLE public.equipment_skill_modifiers_skill_modifier OWNER TO typeormtest;

--
-- Name: equipment_stat_modifiers_stat_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.equipment_stat_modifiers_stat_modifier (
    "equipmentId" integer NOT NULL,
    "statModifierId" integer NOT NULL
);


ALTER TABLE public.equipment_stat_modifiers_stat_modifier OWNER TO typeormtest;

--
-- Name: item; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.item (
    id integer NOT NULL,
    name character varying NOT NULL,
    notes character varying(200) NOT NULL,
    "settingId" integer
);


ALTER TABLE public.item OWNER TO typeormtest;

--
-- Name: item_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_id_seq OWNER TO typeormtest;

--
-- Name: item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.item_id_seq OWNED BY public.item.id;


--
-- Name: race; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.race (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer
);


ALTER TABLE public.race OWNER TO typeormtest;

--
-- Name: race_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.race_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.race_id_seq OWNER TO typeormtest;

--
-- Name: race_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.race_id_seq OWNED BY public.race.id;


--
-- Name: setting; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.setting (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.setting OWNER TO typeormtest;

--
-- Name: setting_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setting_id_seq OWNER TO typeormtest;

--
-- Name: setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.setting_id_seq OWNED BY public.setting.id;


--
-- Name: skill; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.skill (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "statId" integer
);


ALTER TABLE public.skill OWNER TO typeormtest;

--
-- Name: skill_feat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.skill_feat (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "skillId" integer,
    description character varying(2000) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.skill_feat OWNER TO typeormtest;

--
-- Name: skill_feat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.skill_feat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_feat_id_seq OWNER TO typeormtest;

--
-- Name: skill_feat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.skill_feat_id_seq OWNED BY public.skill_feat.id;


--
-- Name: skill_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.skill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_id_seq OWNER TO typeormtest;

--
-- Name: skill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.skill_id_seq OWNED BY public.skill.id;


--
-- Name: skill_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.skill_modifier (
    id integer NOT NULL,
    value integer NOT NULL,
    "skillId" integer
);


ALTER TABLE public.skill_modifier OWNER TO typeormtest;

--
-- Name: skill_modifier_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.skill_modifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_modifier_id_seq OWNER TO typeormtest;

--
-- Name: skill_modifier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.skill_modifier_id_seq OWNED BY public.skill_modifier.id;


--
-- Name: stat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.stat (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer
);


ALTER TABLE public.stat OWNER TO typeormtest;

--
-- Name: stat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.stat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stat_id_seq OWNER TO typeormtest;

--
-- Name: stat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.stat_id_seq OWNED BY public.stat.id;


--
-- Name: stat_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.stat_modifier (
    id integer NOT NULL,
    value integer NOT NULL,
    "statId" integer
);


ALTER TABLE public.stat_modifier OWNER TO typeormtest;

--
-- Name: stat_modifier_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.stat_modifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stat_modifier_id_seq OWNER TO typeormtest;

--
-- Name: stat_modifier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.stat_modifier_id_seq OWNED BY public.stat_modifier.id;


--
-- Name: status_effect; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.status_effect (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "universalModifierId" integer
);


ALTER TABLE public.status_effect OWNER TO typeormtest;

--
-- Name: status_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.status_effect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_effect_id_seq OWNER TO typeormtest;

--
-- Name: status_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.status_effect_id_seq OWNED BY public.status_effect.id;


--
-- Name: status_effect_skill_modifiers_skill_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.status_effect_skill_modifiers_skill_modifier (
    "statusEffectId" integer NOT NULL,
    "skillModifierId" integer NOT NULL
);


ALTER TABLE public.status_effect_skill_modifiers_skill_modifier OWNER TO typeormtest;

--
-- Name: status_effect_stat_modifiers_stat_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.status_effect_stat_modifiers_stat_modifier (
    "statusEffectId" integer NOT NULL,
    "statModifierId" integer NOT NULL
);


ALTER TABLE public.status_effect_stat_modifiers_stat_modifier OWNER TO typeormtest;

--
-- Name: universal_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.universal_modifier (
    id integer NOT NULL,
    hp integer NOT NULL,
    "armorClass" integer NOT NULL,
    "movementSpeed" integer NOT NULL,
    overdose integer NOT NULL,
    universal integer NOT NULL
);


ALTER TABLE public.universal_modifier OWNER TO typeormtest;

--
-- Name: universal_modifier_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.universal_modifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.universal_modifier_id_seq OWNER TO typeormtest;

--
-- Name: universal_modifier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.universal_modifier_id_seq OWNED BY public.universal_modifier.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying NOT NULL,
    uid character varying NOT NULL
);


ALTER TABLE public."user" OWNER TO typeormtest;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO typeormtest;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: character id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character" ALTER COLUMN id SET DEFAULT nextval('public.character_id_seq'::regclass);


--
-- Name: character_class_feat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_class_feat ALTER COLUMN id SET DEFAULT nextval('public.character_class_feat_id_seq'::regclass);


--
-- Name: character_effect id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect ALTER COLUMN id SET DEFAULT nextval('public.character_effect_id_seq'::regclass);


--
-- Name: character_equipment id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment ALTER COLUMN id SET DEFAULT nextval('public.character_equipment_id_seq'::regclass);


--
-- Name: character_item id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item ALTER COLUMN id SET DEFAULT nextval('public.character_item_id_seq'::regclass);


--
-- Name: character_skill id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill ALTER COLUMN id SET DEFAULT nextval('public.character_skill_id_seq'::regclass);


--
-- Name: character_skill_feat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill_feat ALTER COLUMN id SET DEFAULT nextval('public.character_skill_feat_id_seq'::regclass);


--
-- Name: character_stat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat ALTER COLUMN id SET DEFAULT nextval('public.character_stat_id_seq'::regclass);


--
-- Name: class id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class ALTER COLUMN id SET DEFAULT nextval('public.class_id_seq'::regclass);


--
-- Name: class_feat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class_feat ALTER COLUMN id SET DEFAULT nextval('public.class_feat_id_seq'::regclass);


--
-- Name: equipment id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment ALTER COLUMN id SET DEFAULT nextval('public.equipment_id_seq'::regclass);


--
-- Name: item id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.item ALTER COLUMN id SET DEFAULT nextval('public.item_id_seq'::regclass);


--
-- Name: race id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.race ALTER COLUMN id SET DEFAULT nextval('public.race_id_seq'::regclass);


--
-- Name: setting id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.setting ALTER COLUMN id SET DEFAULT nextval('public.setting_id_seq'::regclass);


--
-- Name: skill id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill ALTER COLUMN id SET DEFAULT nextval('public.skill_id_seq'::regclass);


--
-- Name: skill_feat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_feat ALTER COLUMN id SET DEFAULT nextval('public.skill_feat_id_seq'::regclass);


--
-- Name: skill_modifier id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_modifier ALTER COLUMN id SET DEFAULT nextval('public.skill_modifier_id_seq'::regclass);


--
-- Name: stat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat ALTER COLUMN id SET DEFAULT nextval('public.stat_id_seq'::regclass);


--
-- Name: stat_modifier id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat_modifier ALTER COLUMN id SET DEFAULT nextval('public.stat_modifier_id_seq'::regclass);


--
-- Name: status_effect id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect ALTER COLUMN id SET DEFAULT nextval('public.status_effect_id_seq'::regclass);


--
-- Name: universal_modifier id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.universal_modifier ALTER COLUMN id SET DEFAULT nextval('public.universal_modifier_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: character; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public."character" (id, name, hp, "maxHp", "endurantCharges", "userId", "settingId", "raceId", "clazzId", notes, toxicity, hunger) FROM stdin;
2	Conqure	10	10	0	3	1	1	4	**Money**\n$125\n\n**Inspirations**\n-1d4 x 3\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n	0	0
3	Terran "Dos Cadenas" Allen	12	12	1	4	1	3	4	USE OTHER CHARACTER\n	0	0
1	Test	3	5	2	2	1	1	1	**Money**\n$125\n\n**Inspirations**\n-1d4 x 3\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n	3	4
5	Bo of Bo Industries TM	50	12	1	6	1	4	3	**Money**\n$34,125\n\n**Inspirations**\n-1d4 x 4\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n	0	0
4	Terran "Dos Cadenas" Allen	50	12	0	4	1	3	4	**Money**\n$275\n\n**Statuses**\nBloodied\n\n**Inspirations**\n-1d4\n\n**Master Skills**\nRemote Start: You???ve driven your ship enough to pilot it well??? blind! As an action you can pilot your ship remotely.\n\n\n**Inhumanity Score**\n- 5\n\nLast Legs\n--While ???downed??? you may continue acting as if you were alive for 1 + IS turns.Taking damage in this state is considered critical damage. Falling below negative your maximum health is death.\n\nMechanical Internals\n--You may add your IS to overdose checks.\n\nCaloric Tolerance\n--If you possess an IS of 4 you no longer take daily damage from being EMPTY on food.\n\nBody Slam\n--When making a melee strike you can attempt to make an additional attack using your mechanical parts. Make a regular melee attack roll and add your IS. If the attack connects your opponent takes 1d4+IS to 1d6+IS damage.\n\nImpact Compensation\n--Your hydraulic stabilizers reduce fall damage by 50%	0	0
16	Test2	15	15	0	2	1	1	2		0	0
17	ASDF	5	5	0	2	1	2	2		0	0
18	Zauru	50	50	0	32	1	1	2		0	0
19	Zaur	50	50	2	32	1	2	4		0	0
7	Drake fowl	33	11	2	5	1	3	2	**equipped drugs**\n1st:comrades relish\n2nd:brain rot\n3rd:resistance gel\n\n**drug CD's**\ncomrades relish - 5 turns\n\ntoxicity \nhunger 1/1\n\ncurrent daily drug use:\n\n**Money**\n$70\n\n**Inspirations**\n1d4 x2\n\n**Ally points**\n+4 Sunny\n+2 Bo\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n\n- Personalized Medicine\nHit ???em in the groin! Once per encounter, you can exploit an enemies biological weaknesses guarantee a critical strike for an ally???s successful attack on reaction.\n\n- Hold This\nTime apart makes the heart grow fonder. Deposit your weapon inside your enemy and leave it there on a successful attack roll. It deals half damage to them every turn it is lodged inside them. An enemy must make a melee weapon save with a DC of the attack roll you made to remove it, and can repeat that save every turn.\n\n- Detox Expert\nYou???ve invested in a portable dialysis machine capable of filtering even the most toxic of drugs from users. First aid checks can now be used to decrease toxicity. A successful roll (>10) will remove 3 toxicity. A critical success (>15) will remove 5 toxicity.\n\n-Crushing\nIf you land 3 consecutive melee attacks, the final strike is an automatic critical.\n\n-Personal Pharmacist\nYears of frustration with healing solutions on the market have left you frustrated. After hours of research and experimentation, you have discovered powerful alternatives. Takes one hour and the cost of the item to construct.\nSpecialist???s Kit ($25)\n2d4 Heal Consumable\nMorphine Dart ($50)\n1d4 Heal Consumable\n30 ft range\nDefibrillator ($50)\nUse as a reaction on a fatal hit to stay conscious at 1hp on a successful first aid check.\n\n\n\n-CLASS ABILITIES\n\n-All or nothing\nUsing your apparatus you can abuse your own endocrine system to flood your body with adrenaline. While in this state you lose 3hp per turn, but add +5 to all rolls.\n\n-Special Tonic\nCreate a custom concoction of any two existing chemicals. You get both of the effects at once, but both the withdrawals. Max use of the drug per day and toxicity defaults to lowest.\n\n-On Loading\nYou may attempt to steal the current status effects to a target in melee range. You must succeed in an overdose check with a DC of 10 + the toxicity of the target. If you succeed, you gain all of their current status effects and they lose them.\n\n-Burn Out\nReset all cooldowns, but lose all effects. Once per day.\n\n-Carrying giga hammer\n1d10\n10ft range\n\n-carrying a fistful of inactive nanobots from the PPC\n\n\n	7	0
6	Sunny	35	12	3	7	1	5	1	**Actions**\n- 3 actions (movement, items, ability checks)\n - Attacks\n   - 1st attack: normal\n   - 2nd attack: -5\n   - 3rd attack: -10\n- 1 reaction\n\nJump Height: 5ft\n\n**Ally Point**\n- Nataliya +1\n- Drake +4\n\n**SRS**\n- Level 4\n\n**Money**\n- $4\n\n**Inspirations**\n- \n- \n-\n\n**Skills**\n- Shadow Strike\nDangers unseen are twice as deadly. Attack rolls made while sneaking are performed at advantage.\n\n- Hidden In View\nThe best predators can hide in plain sight. You can disappear in combat, mid-conversation or even after being caught.\n\n- Parlor Tricks\nYour weapon of choice is an extension of yourself. Spin it, flip it and toss it. What???s a little danger for some street cred. On a successful attack, the enemy makes a melee weapon save with a DC of 10 + damage dealt and is stunned for a turn if they fail.\n\n- Hold This\nTime apart makes the heart grow fonder. Deposit your weapon inside your enemy and leave it there on a successful attack roll. It deals half damage to them every turn it is lodged inside them. An enemy must make a melee weapon save with a DC of the attack roll you made to remove it, and can repeat that save every turn.\n\n- Specialist\nOne way or another you???ve developed a preference for a particular brand of locks. Roll at advantage with the brand of your choice - Granite: The best of the best.. Though rare, they usually hide incredible valuables\n\n- Concealed Carry\nRoll a steal to use a weapon without being detected. Bystanders may still realize that an attack has been made, but will be unable to connect you to it within reason.\n\n- Monologue\nAs an action in combat, you can attempt to engross an enemy with a powerful monologue. If they fail a persuasion contest with you they are stunned for 1d4 turns. They are allowed to reattempt the contest every stunned turn. You are busy monologuing throughout the stun. While you can take other non-verbal actions, you are not capable of communicating to anyone else while stunning a target this way. This effect also requires line of sight.\n\n- Public Speaking\nYou may add your persuasion modifier twice to any persuasion rolls targeting 3 or more people.\n\n\n\n**Class Skills**\n\n- Battle Meditation\n x2 hunger cost\nGrant all allies (including yourself) within 1 sq mile a single +5 for 1 + SRS turns. Expires at the end of the current encounter.\n\n- Aura: Electric\nWhile active you deplete one hunger per turn. Double your movement speed and jump height.\n\n- Pacify\nDC : Enemy intelligence score + 2\nAs a reaction to an enemy attacking you or an ally, you may attempt to trick them into cancelling their current attack.\n\n\n***NOTES***\n - Peered into soldier's mind to see monsters from Upper City\n  - saw Fish Guy (DEAD), Fancy Girl (Symbient), Wide Chested Man\n  - Long standing deal with Crowley and Upper City\n	0	1
\.


--
-- Data for Name: character_class_feat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_class_feat (id, "characterId", "featId") FROM stdin;
2	1	2
3	1	4
4	1	5
5	1	7
6	1	8
7	1	9
8	1	10
9	1	11
\.


--
-- Data for Name: character_effect; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_effect (id, "characterId", "statusEffectId") FROM stdin;
21	4	5
33	1	5
35	1	13
\.


--
-- Data for Name: character_equipment; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_equipment (id, "isEquipped", "characterId", "equipmentId") FROM stdin;
2	f	2	1
4	t	4	3
3	t	4	4
5	t	4	5
6	t	4	6
1	t	4	1
10	t	5	8
15	t	6	15
24	t	6	22
30	t	4	25
54	t	6	32
66	f	7	36
7	f	7	7
14	f	6	14
16	f	6	16
67	f	6	17
68	t	6	19
26	f	7	20
8	f	7	10
25	t	5	23
11	t	5	9
22	t	5	20
9	t	5	7
71	t	19	5
74	t	19	3
72	t	19	1
76	f	1	3
77	f	1	4
78	f	1	5
79	f	1	6
80	f	1	8
81	f	1	9
82	f	1	11
83	f	1	14
84	f	1	15
85	t	1	35
86	f	1	37
\.


--
-- Data for Name: character_item; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_item (id, quantity, "characterId", "itemId") FROM stdin;
5	1	7	3
6	1	7	4
8	1	7	8
10	1	6	10
16	1	6	14
17	1	6	15
21	1	4	18
22	1	4	14
23	1	4	16
27	1	4	21
28	1	4	22
32	1	7	12
33	1	7	14
34	1	7	16
36	1	7	24
37	1	7	25
35	4	7	34
38	2	7	26
43	1	7	29
44	2	7	30
46	1	7	32
47	3	7	35
48	1	7	33
49	1	1	8
50	1	1	14
52	1	1	16
29	5	1	7
24	0	4	5
25	1	4	6
55	1	7	36
40	4	7	20
11	1	6	7
26	0	4	7
12	1	6	6
39	1	7	19
58	1	6	38
60	1	5	14
63	1	7	37
65	1	7	23
66	3	5	13
68	4	7	13
59	4	6	9
67	2	6	13
69	2	1	40
\.


--
-- Data for Name: character_skill; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_skill (id, experience, "characterId", "skillId") FROM stdin;
151	32	7	1
51	10	3	1
152	0	7	11
52	20	3	11
53	0	3	12
54	0	3	13
55	202	3	14
56	26	3	2
57	449	3	15
58	62	3	7
59	82	3	5
60	26	3	6
61	0	3	4
62	295	3	16
63	40	3	3
64	40	3	10
65	0	3	8
66	20	3	9
67	0	3	17
68	0	3	19
69	0	3	18
70	202	3	20
71	449	3	21
72	0	3	22
73	15	3	23
74	0	3	24
75	0	3	25
33	4	2	16
35	70	2	5
36	0	2	8
78	40	4	3
79	0	4	4
83	0	4	8
84	40	4	10
85	20	4	9
86	20	4	11
87	0	4	12
88	0	4	13
92	0	4	17
93	0	4	18
95	202	4	20
96	449	4	21
98	15	4	23
100	0	4	25
26	1469	2	1
76	23	4	1
153	0	7	12
154	0	7	13
157	0	7	17
159	0	7	18
161	0	7	5
27	34	2	11
28	14	2	12
29	14	2	13
30	14	2	14
31	14	2	15
32	62	2	2
34	10	2	9
37	10	2	7
38	147	2	3
39	15	2	18
40	82	2	4
41	10	2	6
42	15	2	17
102	0	5	11
43	155	2	10
47	15	2	22
45	26	2	20
44	15	2	19
46	15	2	21
48	13	2	23
49	13	2	24
50	13	2	25
104	0	5	13
105	0	5	14
106	0	5	15
112	0	5	8
113	0	5	3
114	0	5	17
115	0	5	4
116	0	5	18
117	0	5	9
118	0	5	22
119	0	5	19
120	0	5	20
122	0	5	21
123	0	5	24
125	0	5	10
126	36	6	1
127	0	6	11
128	0	6	12
129	0	6	13
130	0	6	14
131	0	6	15
134	0	6	17
135	0	6	3
136	0	6	18
138	200	6	9
140	0	6	4
143	56	6	19
144	19	6	5
145	0	6	20
146	0	6	21
147	14	6	22
149	0	6	24
150	0	6	25
164	0	7	4
166	0	7	9
167	0	7	20
168	233	7	21
169	0	7	8
170	0	7	10
173	0	7	24
174	0	7	25
165	42	7	7
142	317	6	8
176	0	2	26
177	0	3	26
179	0	5	26
180	0	6	26
181	0	7	26
132	28	6	16
156	138	7	16
110	43	5	7
81	197	4	6
77	110	4	2
94	19	4	19
178	19	4	26
124	455	5	25
99	15	4	24
171	575	7	22
133	24	6	2
137	644	6	10
139	399	6	6
162	42	7	6
148	571	6	23
97	19	4	22
121	202	5	23
172	10	7	23
158	211	7	2
80	462	4	5
111	184	5	5
109	60	5	2
82	131	4	7
103	1275	5	12
89	268	4	14
91	561	4	16
163	804	7	19
107	42	5	6
160	691	7	3
90	691	4	15
101	212	5	1
141	654	6	7
108	564	5	16
182	150	16	1
183	0	16	17
184	0	16	18
185	0	16	19
186	0	16	22
187	0	16	23
20	1056	1	22
17	1056	1	20
9	1791	1	2
19	1786	1	21
22	1456	1	10
175	972	1	26
21	869	1	23
18	1110	1	8
4	1821	1	13
11	1235	1	4
3	1485	1	12
10	1489	1	17
6	1158	1	15
24	1133	1	24
13	1355	1	3
1	1133	1	1
25	671	1	25
188	0	16	4
189	0	16	7
190	0	16	8
191	0	16	5
192	0	16	9
193	0	16	24
194	0	16	13
195	0	16	12
196	0	16	11
197	0	16	25
198	0	16	16
199	0	16	3
200	0	16	2
201	0	16	6
202	0	16	10
203	0	16	15
204	0	16	21
205	0	16	14
206	0	16	20
207	0	16	26
155	48	7	14
208	150	17	1
209	0	17	17
210	0	17	18
211	0	17	19
212	0	17	22
213	0	17	5
214	0	17	23
215	0	17	7
216	0	17	8
217	0	17	24
218	0	17	4
219	0	17	25
220	0	17	12
224	0	17	2
221	0	17	9
222	0	17	11
223	0	17	3
225	0	17	6
226	0	17	10
227	0	17	13
228	0	17	15
229	0	17	21
230	0	17	14
231	0	17	20
232	0	17	26
233	0	17	16
234	0	18	1
235	0	18	17
236	0	18	18
237	0	18	19
238	0	18	22
239	0	18	23
240	0	18	24
241	0	18	25
242	0	18	4
243	0	18	3
244	0	18	12
245	0	18	2
246	0	18	9
247	0	18	8
248	0	18	7
249	0	18	11
250	0	18	16
251	0	18	6
252	0	18	5
253	0	18	13
254	0	18	15
255	0	18	21
256	0	18	14
257	0	18	20
258	0	18	10
259	0	18	26
261	25	19	17
262	125	19	18
263	125	19	19
264	125	19	22
267	225	19	24
268	150	19	9
269	75	19	11
270	175	19	25
271	125	19	3
273	100	19	4
274	175	19	12
275	62	19	2
272	150	19	8
276	150	19	7
279	125	19	13
280	25	19	15
281	125	19	21
282	25	19	14
283	125	19	20
284	163	19	26
285	50	19	16
266	198	19	5
265	256	19	23
260	38	19	1
278	243	19	10
277	222	19	6
8	1228	1	5
5	1865	1	14
23	1255	1	9
12	1166	1	6
7	1297	1	16
14	1799	1	7
2	1788	1	11
15	1020	1	18
16	953	1	19
\.


--
-- Data for Name: character_skill_feat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_skill_feat (id, "characterId", "featId") FROM stdin;
1	1	1
2	16	2
3	16	1
\.


--
-- Data for Name: character_stat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_stat (id, value, "characterId", "statId") FROM stdin;
1	13	1	1
2	15	1	2
3	7	1	3
4	14	1	4
5	15	1	5
6	11	1	7
7	12	1	6
8	12	2	1
9	10	2	2
10	6	2	3
11	19	2	4
12	10	2	5
13	11	2	6
14	9	2	7
15	13	3	1
16	21	3	2
17	15	3	3
18	13	3	4
19	22	3	5
20	19	3	6
21	15	3	7
22	13	4	1
23	21	4	2
24	15	4	3
25	13	4	4
26	22	4	5
27	19	4	6
28	15	4	7
29	12	5	1
30	14	5	2
31	17	5	3
32	8	5	4
33	17	5	5
34	16	5	6
35	21	5	7
36	12	6	1
37	19	6	2
38	15	6	3
39	12	6	4
40	14	6	5
41	14	6	6
42	20	6	7
43	16	7	1
44	14	7	2
45	14	7	3
46	16	7	4
47	16	7	5
48	23	7	6
49	7	7	7
50	15	16	1
51	15	16	2
52	15	16	3
53	15	16	4
54	15	16	5
55	15	16	6
56	15	16	7
57	12	17	1
58	14	17	2
59	11	17	3
60	11	17	5
61	11	17	6
62	11	17	7
63	12	17	4
64	13	18	1
65	14	18	2
66	13	18	3
67	0	18	4
68	0	18	5
69	0	18	6
70	0	18	7
71	13	19	1
72	16	19	2
73	13	19	3
74	16	19	4
75	8	19	5
76	14	19	6
77	16	19	7
\.


--
-- Data for Name: class; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.class (id, name, "settingId") FROM stdin;
1	Symbient	1
2	Juicer	1
3	Cipher	1
4	Machinist	1
\.


--
-- Data for Name: class_feat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.class_feat (id, name, "settingId", "clazzId", description) FROM stdin;
2	Battle Meditation	1	1	 x2 hunger cost\nGrant all allies (including yourself) within 1 sq mile a single +5 for 1 + SRS turns. Expires at the end of the current encounter.
3	Sleight of Mind	1	1	Add 2 + your symbient relation score to skill checks or DCs.\nCan be performed on any checks besides damage rolls and overdose checks.
4	Aura: Electric	1	1	While active you deplete one hunger per turn. Double your movement speed and jump height.
5	Stasis	1	1	Freeze your enemy in place. Enemies who fail a strength save with a DC of 10 + your SRS are rendered motionless for 1 + SRS turns. 
6	Overload	1	1	x2 hunger cost\nDischarge a psychic explosion. Though costly, this attack more than makes up for it with its damage. However, it does not distinguish between friend or foe. Use wisely. (Cannot be used without potentially damaged allyΓÇÖs consent). Enemies within 20ft take 1d8 + your SRS damage. Enemies who pass a dexterity save with a DC of 10 + your SRS take half damage. If they fail the dexterity save, the enemy is shocked for their next turn.
7	Splinter	1	1	Choose any destructible object within 50ft and turn it into shrapnel. Enemies within 15ft of the object take 1d4 + SRS physical damage.
8	Order	1	1	DC : Enemy intelligence score\nUse an action to order an enemy to move in any particular direction up to half their total movement speed.
9	Disarm	1	1	DC : Enemy intelligence score + 2\nUse an action to order an enemy to drop their weapon.
10	Pacify	1	1	DC : Enemy intelligence score + 2\nAs a reaction to an enemy attacking you or an ally, you may attempt to trick them into cancelling their current attack.
11	Betrayal	1	1	DC : Enemy intelligence score + 4\nOn an enemy's turn, use a reaction to attempt to trick them into temporarily betraying their allies. If successful, you hijack their actions and movement.\n
12	Burn Out	1	2	Reset all cooldowns, but lose all effects. Once per day.\n
13	Off Loading	1	2	You may attempt to off load your current status effects to a target in melee range. The target must succeed in an overdose check with a DC of 10 + your toxicity you currently have. If they fail, they gain all of your current status effects and you lose them.
14	On Loading	1	2	You may attempt to steal the current status effects to a target in melee range. You must succeed in an overdose check with a DC of 10 + the toxicity of the target. If you succeed, you gain all of their current status effects and they lose them.
15	Type-O Transfusion	1	2	For the cost of an action you can transfer your health 1:1 to a willing target.
16	Special Tonic	1	2	Create a custom concoction of any two existing chemicals. You get both of the effects at once, but both the withdrawals. \n\nMax use of the drug per day and toxicity defaults to lowest.
17	Antidote	1	2	You can analyze the chemical composition of a chemical and derive an antidote. Roll a percentile dice and if the result passes the drugs DC (determined by DM prior to roll) then you assemble an antidote.
18	All or Nothing	1	2	Using your apparatus you can abuse your own endocrine system to flood your body with adrenaline. While in this state you lose 3hp per turn, but add +5 to all rolls.
\.


--
-- Data for Name: equipment; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.equipment (id, name, "settingId", "universalModifierId", notes) FROM stdin;
1	Kevlite	1	1	
7	Leather Jacket	1	7	
10	Bonesaw	1	10	
21	Child's Fur Lined Jacket	1	21	
20	Jogging Pants	1	20	
32	Taser Blade	1	42	
36	alcoholic metabolizer	1	66	
17	Deflection Bracers	1	17	These white bracers ring with anticipation when high velocity projectiles fly through its space. As a reaction to a projectileΓÇÖs path coming within 5ft of you make a dexterity check with DC of the 10 + projectile's damage. If youΓÇÖre successful you can use the projectile to make a melee weapons attack roll against an enemy with 25ft.
18	Charged Circlet	1	18	This statically charged headband gives you the ability to use telekinesis. Disarm an enemy. Hold a door closed. Push an enemy up to 10ft in any direction.\n\nRange : 50ft
19	Chat Ring	1	19	While equipped you can speak to any ally also wearing a chat ring within 10 sq miles.
22	Electrolysis Band	1	22	This band allows you to subtly use electrotherapy on the targetΓÇÖs body. You can make a SRS check with a DC of 10 to remove a toxin of your choosing from their bloodstream. If you succeed the check you may remove any status effect on your target with their consent.\n\n5ft range / Touch
3	Hydraulic Stabilizers	1	3	Legs\n\n12 Hack Resistance\n\nThese titanium alloy legs are powered by a state of the art Krull (tm) hydraulic oil that pushes their strength to the utmost limits of fluid powered mechanics. Though they have the added benefit of improving the balance of their user, their primary function is siege and destruction. One kick is all it takes.\n\nKick\n1d10 physical damage\n1d10 siege damage\n5 turn cooldown
4	Personal Fusion Core	1	4	Chest\n\n16 Hack Resistance\n\nA compartmentalized fusion core installed in your torso. The power generated increases your strength and constitution and can be overloaded to ignite a localized nuclear blast.\n\nNuke Ignition\n2d6 fire damage\n1d6 siege damage\n1 day cooldown\n
5	Seeker Rifle	1	5	Precision Gun\n\nThis high-tech corporate assault rifle has a biometric lock on its trigger. Perhaps a tech pirate could rig it for inappropriate use.\n\n1d12 physical damage, 1d8 critical fire damage\n50ft range
6	Grasping Hands	1	6	Your hands are replaced with a magnetic, liquid metal that you can control. You can stretch them into many different shapes, and they are far denser than your old skin and bone.
8	Microwave Toolkit	1	8	This weaponized kitchen appliance is deadly at short range, and builds momentum upon sequential hits. Each successful hit in a row increases your damage and critical chance by 5%. \n\nCritical strikes count as two hits. Missing a hit or not attacking resets your bonus.\n\n1d6 fire damage, 1d8 fire critical damage\n20ft range\n
9	AI Combat Visor	1	9	The AI in this visorΓÇÖs microchip is capable of hyper analyzing an enemy foe. If a foe is struck by anyone in your line of sight you may use a reaction to study their weaknesses. \n\nYour next successful attack on them will be a critical strike. This foe also has disadvantage when attacking you directly.
11	Boomerang Blade	1	11	A curved blade that can be thrown. When thrown, it will always return to the wielder, but itΓÇÖs user must make a melee weapon save of DC 10 to catch the blade. If they fail they take 1d4 physical damage. \n\nCritical damage can be distributed to two enemies within range.\n\n1d6 physical damage\n5ft range, 30ft throw range
14	Spectral Armor	1	14	A highly sophisticated, one-of-a-kind suit.\n\nCan go invisible. Add 1d4 to stealth rolls.\n
15	HoloMask	1	15	Though it provides little physical defense, the HoloMask is a one-size-fits-all solution to surveillance avoidance and stealth. Project a disorienting matrix onto your face that confuses the human eye and facial recognition algorithms.
16	Lightning Glove	1	16	The bulky, wire mesh glove has long curved, dagger-like fingertips that channel lightning frighteningly well. Up close they are a terror, at a distance.. a nightmare.\n\nMelee Attack\nRend your foes with your dagger tipped hands.\n1d6 physical damage, 1d6 electrical critical damage.\n\nRanged Attack\nStrike your foes with lightning that arcs to two additional targets on a critical strike.\n1d8 electrical damage, 1d8 (2-spread) electrical critical damage\n15ft\n
23	Electromagnetic Coil	1	23	This chest piece can be activated as a reaction to attempt to redirect any projectiles within 30ft. You must roll a hack check with a DC of 10 + the maximum damage of the projectile. If successful you can use the roll as an attack roll against any enemy within 50ft.\n
24	Boombox Boots	1	24	These sneakers are each installed with supersonic speakers on their soles. The sound waves produced by these shoes allow the user to see through walls up to 50ft.
25	Threaded Band	1	25	Head\n\n8 Hack Resistance\n\nThis metal bar runs horizontally, chemically bonded to the skin on your head. It uses electrical signals to improve your reaction speed. 
33	M-11	1	52	Standard Issue. Point and shoot.\n\n1d6 physical damage\n25ft range
34	Morphine Dart	1	53	1d4 Heal Consumable\n30 ft range
35	Peta Sickle	1	63	When thrown it can attack up to three targets. On a successful attack the target becomes bloodied until healed. Bloodied enemies take +SRS damage. Symbients may use an action to recall this weapon to their hand.\n\n1d4 physical damage.\n
37	Grappling Arm	1	70	Arm\n\n14 Hack Resistance\n\nThis spring-loaded arm can either be used to grapple others from afar or to fly to otherwise unreachable terrain. \n\n50ft range\n
\.


--
-- Data for Name: equipment_skill_modifiers_skill_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.equipment_skill_modifiers_skill_modifier ("equipmentId", "skillModifierId") FROM stdin;
6	1
6	2
8	3
9	4
10	5
16	6
22	7
37	8
37	9
\.


--
-- Data for Name: equipment_stat_modifiers_stat_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.equipment_stat_modifiers_stat_modifier ("equipmentId", "statModifierId") FROM stdin;
1	2
3	6
3	7
4	9
4	11
7	24
9	26
20	36
21	38
22	39
23	40
24	41
25	42
37	55
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.item (id, name, notes, "settingId") FROM stdin;
3	Gen Spice	A dose of Orange sparkly liquid that gives a burst of energy	1
4	Eye Opener	A dose of pale blue liquid, aids brain function for a short time	1
5	Incinerator Grenade	15 ft Radius\n1d6 fire damage\n	1
6	Emergency Kits	1d4 healing	1
7	Specialist's Kit	2d4 healing	1
8	Flex Juice	A dose of glowing red liquid, gives a tremendous boost to physical capabilities. *WARNING*: overdose can be fatal!	1
9	Generic Pre-Pack	1x food	1
10	Deckard's Brain	a dog brain in a jar	1
12	Exotic Bar: Guava Berry	+3 Strength\n+1 Food	1
13	Dine-in Ready Meal	x2 Food\n+2 Strength	1
14	Exotic Bar: Pomenango	+3 Charisma\n+1 Food	1
15	Exotic Bar: CocoNutty	+2 CON\n+2 HP\nx1 Food	1
16	Exotic Bar: Chocoholic	-1 Dexterity\n+2 Food	1
19	Resistance Gel	+5 toxicity\nAs a reaction heal back half of received damage\n1 Turn\nOverdose Effects\n-1 HP\nMax Per Day. 1	1
20	Scalpels	1d4 physical dmg	1
21	Morphine Dart	1d4 heal\n30 ft range\n	1
22	Defibrillator	Use as a reaction on a fatal hit to stay conscious at 1hp on a successful first aid check.	1
23	Uzi	2d4 physical damage, 1d8 physical critical damage\n20ft range	1
24	Day Dream	I FEEL FANTASTIC\nAlso causes terrible hangover	1
25	Ash Virus	vials of mystery drug	1
27	Defibrilator	first aid pass reaction on fatal hit to come back at 1hp	1
28	Specialist Kit	2d4 heal consumable	1
29	Roid-it	+4 toxicity\nRe-roll an ability check or attack.\nOverdose Effects\n1d10 poison damage	1
30	Speed Gum	+2 toxicity\n+20 ft of movement\nOverdose Effects\nYou gain a limp\n1d4 poison damage	1
31	Desert Spice	+3 toxicity\nYou gain the ability to spit fire.\n1d4 fire damage\n2 Turns\nOverdose Effects\n1d6 fire damage	1
32	Cretid Venom	+3 Toxicity\n+2 Strength\n-3 Intelligence\n3 Turns\nOverdose Effects\n-2 HP\n-5 Intelligence\nMax Per Day. 0	1
33	Brain Rot	+3 Toxicity\n+2 Strength\n+1 intelligence \n+1 Wisdom\n3 Turns\nOverdose Effects\n-9 Intelligence\n-2 HP\nMax per day: 0	1
34	Mystery Serum	Cures the ash virus	1
35	Mystery Numbing liquid	Strange medical numbing liquid used to preserve dog brains	1
26	Neo-Coke	+1 Intelligence\n+1 food	1
18	Equality Apple Pie	+1 to all rolls until expiration	1
36	Widows Venom	.+2 STR\n.+5 CHR\n.-3 INT\n3 Turns\nOverdose Effects\n-2 HP\n-5 INT\n1 day coma\nMax: 0/Day\nToxicity: 3	1
37	Comrades Relish	+5 toxicity\n+5 Strength\n1 Turn\nOverdose Effects\n-30 Movement Speed\n-5 Max Health\nMax Per Day. 1\n($50)	1
38	Superior Kit	3d4 healing	1
39	TestItem4	asdf	1
40	Sudo-42	A mysterious thick, silvery, fluid. Collected from the lair of Sudogoyles.	1
\.


--
-- Data for Name: race; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.race (id, name, "settingId") FROM stdin;
1	Old Worlder	1
2	Peasant	1
3	Tecchi	1
4	Noble	1
5	Patrician	1
\.


--
-- Data for Name: setting; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.setting (id, name, description) FROM stdin;
1	Tronix	Cyberpunk
\.


--
-- Data for Name: skill; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.skill (id, name, "settingId", "statId") FROM stdin;
1	Heavy Guns	1	1
4	Throwing	1	1
5	Precision Guns	1	2
7	Sneak	1	2
8	Lockpick	1	2
9	Steal	1	2
11	Energy Weapons	1	5
12	Hacking	1	5
13	Traps	1	5
16	Personal Piloting	1	6
17	Freighter Piloting	1	6
18	Diagnostics	1	6
19	First Aid	1	6
22	Surgery	1	6
23	Persuasion	1	7
24	Intimidation	1	7
25	Trade	1	7
3	Melee Weapons (Str)	1	1
2	Unarmed (Str)	1	1
6	Unarmed (Dex)	1	2
10	Melee Weapons (Dex)	1	2
15	Repair (Int)	1	5
21	Repair (Wis)	1	6
14	Engineering (Int)	1	5
20	Engineering (Wis)	1	6
26	Searching	1	6
\.


--
-- Data for Name: skill_feat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.skill_feat (id, name, "settingId", "skillId", description) FROM stdin;
1	Mercantile Reputation	1	25	
2	Built In Your Own Image	1	21	On a successful repair roll, you may make a modification to the repaired device of your choosing (within reason).
\.


--
-- Data for Name: skill_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.skill_modifier (id, value, "skillId") FROM stdin;
1	3	8
2	7	9
3	1	12
4	1	12
5	2	22
6	1	24
7	2	19
8	1	2
9	1	6
\.


--
-- Data for Name: stat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.stat (id, name, "settingId") FROM stdin;
1	Strength	1
2	Dexterity	1
3	Constitution	1
4	Endurance	1
5	Intelligence	1
6	Wisdom	1
7	Charisma	1
\.


--
-- Data for Name: stat_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.stat_modifier (id, value, "statId") FROM stdin;
2	-1	7
4	-1	7
6	2	2
7	2	4
9	1	1
11	1	3
24	2	7
26	1	5
28	3	1
29	3	2
30	-10	1
32	-10	1
34	1	1
36	1	4
38	1	7
39	1	3
40	1	2
41	1	2
42	1	2
43	2	1
44	1	5
45	1	6
47	1	5
48	5	1
49	2	1
50	3	1
51	-1	2
52	2	3
53	3	7
54	1	3
55	1	1
56	1	5
57	1	6
\.


--
-- Data for Name: status_effect; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.status_effect (id, name, "settingId", "universalModifierId") FROM stdin;
1	Brain Rot	1	26
3	Flex juice	1	43
4	Flex Juice 2	1	44
5	Equality Apple Pie	1	51
6	Neo-Coke	1	54
7	All or Nothing	1	55
8	Comrades Relish	1	61
9	Overdose: Comrades Relish	1	62
10	Strained Heart	1	65
11	Dine-in Ready Meal	1	67
12	Ambrosia Booze	1	68
13	Eye Opener	1	71
\.


--
-- Data for Name: status_effect_skill_modifiers_skill_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.status_effect_skill_modifiers_skill_modifier ("statusEffectId", "skillModifierId") FROM stdin;
\.


--
-- Data for Name: status_effect_stat_modifiers_stat_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.status_effect_stat_modifiers_stat_modifier ("statusEffectId", "statModifierId") FROM stdin;
1	43
1	44
1	45
6	47
8	48
11	49
12	50
12	51
12	52
12	53
13	56
13	57
\.


--
-- Data for Name: universal_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.universal_modifier (id, hp, "armorClass", "movementSpeed", overdose, universal) FROM stdin;
1	0	2	0	0	0
2	0	2	0	0	0
3	0	0	0	0	0
4	0	0	0	0	0
5	0	0	0	0	0
6	0	0	0	0	0
7	0	1	0	0	0
8	0	0	0	0	0
9	0	0	0	0	0
10	0	0	0	0	0
11	0	0	0	0	0
12	0	0	0	0	0
13	0	0	0	0	0
14	0	3	0	0	0
15	0	0	0	0	0
16	0	1	0	0	0
17	0	0	0	0	0
18	0	0	0	0	0
19	0	0	0	0	0
20	0	1	0	0	0
21	0	0	0	0	0
22	0	0	0	0	0
23	0	2	0	0	0
24	0	1	0	0	0
25	0	2	0	0	0
26	0	0	0	0	0
27	0	0	0	0	0
28	0	0	0	0	0
29	1	0	0	0	0
30	0	0	0	0	0
31	0	0	0	0	15
32	0	0	0	0	7
33	0	0	0	0	0
34	0	0	0	0	0
35	0	0	0	0	0
36	0	0	0	0	0
37	0	0	0	0	0
38	0	0	0	0	0
39	0	0	0	0	0
40	0	0	0	0	0
41	0	0	0	0	0
42	0	0	0	0	0
43	0	0	0	0	0
44	0	0	0	0	0
45	0	0	0	0	0
46	0	0	0	0	0
47	0	0	0	0	0
48	0	0	0	0	0
49	0	0	0	0	0
50	0	0	0	0	0
51	0	0	0	0	1
52	0	0	0	0	0
53	0	0	0	0	0
54	0	0	0	0	0
55	0	0	0	0	5
56	0	0	0	0	0
57	0	0	0	0	0
58	0	0	0	0	0
59	0	0	0	0	0
60	0	0	0	0	0
61	0	0	0	0	0
62	-5	0	-30	0	0
63	0	0	0	0	0
64	0	0	0	0	0
65	-6	0	0	0	0
66	0	0	0	0	0
67	0	0	0	0	0
68	2	0	0	0	0
69	0	0	0	1	0
70	0	0	0	0	0
71	0	0	0	0	0
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public."user" (id, email, uid) FROM stdin;
1	Jeffy@amazon.com	Zv1NPmF8jjXJzkOZs0ceL4gfyPT2
2	justinrwalters95@gmail.com	RM03e7cUsZRO4Ow9I4JVl0CLjAj1
3	jakewaltersss@gmail.com	yWIjNl4jGah4Gw1yVgw16Szn4DT2
4	treyh8794@hotmail.com	iloC77IMhVQt38GknVcKBBA1qbk2
5	jakewoodall1120@gmail.com	AREnPW13fFQwldg7xqQSP62o2Nl2
6	bhunter2394@gmail.com	Qi7G8d3AevXmxp1GS90HUjE1UW13
7	hjwalters21@gmail.com	PPLcLUTEGINTObiV8DXO2KU0FGA3
8	Yasmeen76@gmail.com	3Lmb4MbL66Szq8XxRDWzMRy3tj43
9	man@gmail.com	HbFPOSSWzgadkDeeVTjidR7P28n1
10	datBoi@datboiserver.com	k1JE4AUnGPMRkFAJrCFGzxRH3Xz2
11	indininya@yahoo.com	McgJR8dcTSenrqjLhCyIpMGQuD23
12	halltrina12@yahoo.com	sdEh3FV9KSNLryAN7swIdg413302
13	aclick@nrtc.coop	vyralBkCnvVEyCu4t87fVXwDx1b2
14	wairose254@gmail.com	jE7mLUVND8WQp7QiJnktHwiP05F2
15	dracochi45@gmail.com	bTiBdQMpfIQODfjdPePNzBfZYKU2
16	taylordonell638@gmail.com	ldSgJ5BMDIZyXyZ9xl3miHanhpf2
17	Jade_Weissnat2@hotmail.com	RlKP9UEvelcXG2Wqwcil6PExWtA2
18	Mae.Welch@gmail.com	94U5zLUJr1NZIqqtMi6gHxgtF3z1
19	ktemple720@yahoo.com	mNxhruJ3joMeGAYbcN3toAKIZ4I3
20	monikkiyo@gmail.com	E9fwahvIeGaJKSRU4JJ2zqyUghD3
21	jenniferllewis79@yahoo.com	drjgqESq3KV2QLUCb9BIgWCGkLs2
22	3amegos321@gmail.com	IF0DrBy5YXeSw5ULvMHQLW7JhEM2
23	misey.hul@gmail.com	BqvHMB7zLbXTT7scRbCybR0FCaU2
24	michaelmasoner@yahoo.com	fkhILcG186eC5Wqb7gYStNoVLjG3
25	carlaveliz2003@yahoo.com	dEJy0A8RIMdiXjzSQgfo38Snnlz1
26	destinicd@gmail.com	OyHK5mYLuLSHcj323end66pMR1q1
27	kheesern.kwan@tennantco.com	vRHye53OcAcwYR1PFVqUjs1l3TU2
28	julialward1@gmail.com	iRsHp39rWed6mIFKvFIXbCUYGjQ2
29	info@influnate.io	X6LJwnxAqTZcUuLTlC1y2ha8PCW2
30	sunnyychunn@gmail.com	ZHfo8KeW6pcafeihslwlI7FOAoY2
31	peytonshap@gmail.com	b1phwI1rUMeRQbNzk25cp4Vb4bj1
32	zwburris73@gmail.com	L8mWU0H1HiVKvyR3bKxcebOkki83
33	tlind599@gmail.com	er77MokscpTd5hUDHD1sR1Dukju2
34	transformers611@hotmail.com	zqJC64rc6zZhQPex55vBczbyvl83
\.


--
-- Name: character_class_feat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_class_feat_id_seq', 9, true);


--
-- Name: character_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_effect_id_seq', 35, true);


--
-- Name: character_equipment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_equipment_id_seq', 86, true);


--
-- Name: character_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_id_seq', 19, true);


--
-- Name: character_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_item_id_seq', 69, true);


--
-- Name: character_skill_feat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_skill_feat_id_seq', 3, true);


--
-- Name: character_skill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_skill_id_seq', 285, true);


--
-- Name: character_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_stat_id_seq', 77, true);


--
-- Name: class_feat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.class_feat_id_seq', 18, true);


--
-- Name: class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.class_id_seq', 4, true);


--
-- Name: equipment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.equipment_id_seq', 37, true);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.item_id_seq', 40, true);


--
-- Name: race_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.race_id_seq', 5, true);


--
-- Name: setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.setting_id_seq', 1, true);


--
-- Name: skill_feat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.skill_feat_id_seq', 2, true);


--
-- Name: skill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.skill_id_seq', 26, true);


--
-- Name: skill_modifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.skill_modifier_id_seq', 9, true);


--
-- Name: stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.stat_id_seq', 7, true);


--
-- Name: stat_modifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.stat_modifier_id_seq', 57, true);


--
-- Name: status_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.status_effect_id_seq', 13, true);


--
-- Name: universal_modifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.universal_modifier_id_seq', 71, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.user_id_seq', 34, true);


--
-- Name: equipment PK_0722e1b9d6eb19f5874c1678740; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT "PK_0722e1b9d6eb19f5874c1678740" PRIMARY KEY (id);


--
-- Name: universal_modifier PK_078b14b5aa1511cc02850035b1b; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.universal_modifier
    ADD CONSTRAINT "PK_078b14b5aa1511cc02850035b1b" PRIMARY KEY (id);


--
-- Name: class PK_0b9024d21bdfba8b1bd1c300eae; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class
    ADD CONSTRAINT "PK_0b9024d21bdfba8b1bd1c300eae" PRIMARY KEY (id);


--
-- Name: stat PK_132de903d366f4c06cd586c43c0; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat
    ADD CONSTRAINT "PK_132de903d366f4c06cd586c43c0" PRIMARY KEY (id);


--
-- Name: skill_modifier PK_360bb661107c161b70adaa377d2; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_modifier
    ADD CONSTRAINT "PK_360bb661107c161b70adaa377d2" PRIMARY KEY (id);


--
-- Name: status_effect_skill_modifiers_skill_modifier PK_3b5e6bdbf152b98fbf8450e481a; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_skill_modifiers_skill_modifier
    ADD CONSTRAINT "PK_3b5e6bdbf152b98fbf8450e481a" PRIMARY KEY ("statusEffectId", "skillModifierId");


--
-- Name: skill_feat PK_432d30d2b55cf60d399d705573e; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_feat
    ADD CONSTRAINT "PK_432d30d2b55cf60d399d705573e" PRIMARY KEY (id);


--
-- Name: character_equipment PK_4afeeea248417ce2474b06820d9; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment
    ADD CONSTRAINT "PK_4afeeea248417ce2474b06820d9" PRIMARY KEY (id);


--
-- Name: character_effect PK_4fabba72f75951da75ace73c5bf; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect
    ADD CONSTRAINT "PK_4fabba72f75951da75ace73c5bf" PRIMARY KEY (id);


--
-- Name: character PK_6c4aec48c564968be15078b8ae5; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "PK_6c4aec48c564968be15078b8ae5" PRIMARY KEY (id);


--
-- Name: class_feat PK_74b4ae59d094a4f3191a66d0e3a; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class_feat
    ADD CONSTRAINT "PK_74b4ae59d094a4f3191a66d0e3a" PRIMARY KEY (id);


--
-- Name: character_stat PK_7a7db5bbafa5832f6b8ee8feae7; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat
    ADD CONSTRAINT "PK_7a7db5bbafa5832f6b8ee8feae7" PRIMARY KEY (id);


--
-- Name: character_skill PK_835ce87665bea249a62f1a706c1; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill
    ADD CONSTRAINT "PK_835ce87665bea249a62f1a706c1" PRIMARY KEY (id);


--
-- Name: character_skill_feat PK_83b5eaf1e40594263af01dc9d9e; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill_feat
    ADD CONSTRAINT "PK_83b5eaf1e40594263af01dc9d9e" PRIMARY KEY (id);


--
-- Name: status_effect_stat_modifiers_stat_modifier PK_8cfa38a4574b63412e1afaaeda1; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_stat_modifiers_stat_modifier
    ADD CONSTRAINT "PK_8cfa38a4574b63412e1afaaeda1" PRIMARY KEY ("statusEffectId", "statModifierId");


--
-- Name: status_effect PK_9156e6e7704d054b364793ec3d4; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect
    ADD CONSTRAINT "PK_9156e6e7704d054b364793ec3d4" PRIMARY KEY (id);


--
-- Name: stat_modifier PK_96c2a5228fde4bc5ace781171d6; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat_modifier
    ADD CONSTRAINT "PK_96c2a5228fde4bc5ace781171d6" PRIMARY KEY (id);


--
-- Name: skill PK_a0d33334424e64fb78dc3ce7196; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT "PK_a0d33334424e64fb78dc3ce7196" PRIMARY KEY (id);


--
-- Name: race PK_a3068b184130d87a20e516045bb; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.race
    ADD CONSTRAINT "PK_a3068b184130d87a20e516045bb" PRIMARY KEY (id);


--
-- Name: character_class_feat PK_b64f40a6a0c1e473969dccf0253; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_class_feat
    ADD CONSTRAINT "PK_b64f40a6a0c1e473969dccf0253" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: item PK_d3c0c71f23e7adcf952a1d13423; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT "PK_d3c0c71f23e7adcf952a1d13423" PRIMARY KEY (id);


--
-- Name: equipment_skill_modifiers_skill_modifier PK_e13eda2e618db444b68f8e6f947; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_skill_modifiers_skill_modifier
    ADD CONSTRAINT "PK_e13eda2e618db444b68f8e6f947" PRIMARY KEY ("equipmentId", "skillModifierId");


--
-- Name: character_item PK_e4a897636d3db088f47702415a5; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item
    ADD CONSTRAINT "PK_e4a897636d3db088f47702415a5" PRIMARY KEY (id);


--
-- Name: equipment_stat_modifiers_stat_modifier PK_f4c6ab8bafa79d50f0e49545d07; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_stat_modifiers_stat_modifier
    ADD CONSTRAINT "PK_f4c6ab8bafa79d50f0e49545d07" PRIMARY KEY ("equipmentId", "statModifierId");


--
-- Name: setting PK_fcb21187dc6094e24a48f677bed; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.setting
    ADD CONSTRAINT "PK_fcb21187dc6094e24a48f677bed" PRIMARY KEY (id);


--
-- Name: IDX_0fb00fdc0806db73fa82e788c4; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_0fb00fdc0806db73fa82e788c4" ON public.status_effect_skill_modifiers_skill_modifier USING btree ("statusEffectId");


--
-- Name: IDX_459e9d51fc50c2d2c63e0d2fb2; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_459e9d51fc50c2d2c63e0d2fb2" ON public.status_effect_stat_modifiers_stat_modifier USING btree ("statusEffectId");


--
-- Name: IDX_5be50d8fcc7a8ae57732aabd93; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_5be50d8fcc7a8ae57732aabd93" ON public.status_effect_skill_modifiers_skill_modifier USING btree ("skillModifierId");


--
-- Name: IDX_5d6dbff6be99f1f751ddd25bdb; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_5d6dbff6be99f1f751ddd25bdb" ON public.equipment_stat_modifiers_stat_modifier USING btree ("equipmentId");


--
-- Name: IDX_90b8d41223816f64c0d1e23c1f; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_90b8d41223816f64c0d1e23c1f" ON public.equipment_skill_modifiers_skill_modifier USING btree ("skillModifierId");


--
-- Name: IDX_b21f62444df75b2bf76d0eeea6; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_b21f62444df75b2bf76d0eeea6" ON public.status_effect_stat_modifiers_stat_modifier USING btree ("statModifierId");


--
-- Name: IDX_bb9c6775cea83d5c0decb49848; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_bb9c6775cea83d5c0decb49848" ON public.equipment_stat_modifiers_stat_modifier USING btree ("statModifierId");


--
-- Name: IDX_e73e4d8a0f2ae9a1816968fdc4; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_e73e4d8a0f2ae9a1816968fdc4" ON public.equipment_skill_modifiers_skill_modifier USING btree ("equipmentId");


--
-- Name: character FK_04c2fb52adfa5265763de8c4464; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_04c2fb52adfa5265763de8c4464" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: character_class_feat FK_0d2c05ec3296203865734893b54; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_class_feat
    ADD CONSTRAINT "FK_0d2c05ec3296203865734893b54" FOREIGN KEY ("featId") REFERENCES public.class_feat(id);


--
-- Name: character_equipment FK_0f0b9eee1410967891a80a64d24; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment
    ADD CONSTRAINT "FK_0f0b9eee1410967891a80a64d24" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: status_effect_skill_modifiers_skill_modifier FK_0fb00fdc0806db73fa82e788c44; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_0fb00fdc0806db73fa82e788c44" FOREIGN KEY ("statusEffectId") REFERENCES public.status_effect(id) ON DELETE CASCADE;


--
-- Name: class_feat FK_23d202d6c017bf6385585b66d05; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class_feat
    ADD CONSTRAINT "FK_23d202d6c017bf6385585b66d05" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: class_feat FK_2bbc14d5a0b84d70c53a007d43f; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class_feat
    ADD CONSTRAINT "FK_2bbc14d5a0b84d70c53a007d43f" FOREIGN KEY ("clazzId") REFERENCES public.class(id);


--
-- Name: skill FK_326ef0a6800b4254fda64ca792d; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT "FK_326ef0a6800b4254fda64ca792d" FOREIGN KEY ("statId") REFERENCES public.stat(id);


--
-- Name: class FK_37717e4f894378dfd3105dd62ff; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class
    ADD CONSTRAINT "FK_37717e4f894378dfd3105dd62ff" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: character_stat FK_3829d539757783ad28f25c74c97; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat
    ADD CONSTRAINT "FK_3829d539757783ad28f25c74c97" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character_skill FK_39dcbdc540ee9ff68c7e39bcbe4; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill
    ADD CONSTRAINT "FK_39dcbdc540ee9ff68c7e39bcbe4" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character_equipment FK_406f5846e5afe5c32c03a94e604; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment
    ADD CONSTRAINT "FK_406f5846e5afe5c32c03a94e604" FOREIGN KEY ("equipmentId") REFERENCES public.equipment(id);


--
-- Name: status_effect_stat_modifiers_stat_modifier FK_459e9d51fc50c2d2c63e0d2fb25; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_459e9d51fc50c2d2c63e0d2fb25" FOREIGN KEY ("statusEffectId") REFERENCES public.status_effect(id) ON DELETE CASCADE;


--
-- Name: race FK_509e14d0693cd13e63114746334; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.race
    ADD CONSTRAINT "FK_509e14d0693cd13e63114746334" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: skill FK_5728d17b7708e3ac07a9c1382c7; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT "FK_5728d17b7708e3ac07a9c1382c7" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: status_effect_skill_modifiers_skill_modifier FK_5be50d8fcc7a8ae57732aabd93f; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_5be50d8fcc7a8ae57732aabd93f" FOREIGN KEY ("skillModifierId") REFERENCES public.skill_modifier(id) ON DELETE CASCADE;


--
-- Name: equipment_stat_modifiers_stat_modifier FK_5d6dbff6be99f1f751ddd25bdb9; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_5d6dbff6be99f1f751ddd25bdb9" FOREIGN KEY ("equipmentId") REFERENCES public.equipment(id) ON DELETE CASCADE;


--
-- Name: character_class_feat FK_5f7366d6d3c7577c4ce27ae5e8e; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_class_feat
    ADD CONSTRAINT "FK_5f7366d6d3c7577c4ce27ae5e8e" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character_effect FK_6598eb68c73c94cf2928f28d1fc; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect
    ADD CONSTRAINT "FK_6598eb68c73c94cf2928f28d1fc" FOREIGN KEY ("statusEffectId") REFERENCES public.status_effect(id);


--
-- Name: character_effect FK_6f8d32646cd388c667b27e5135d; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect
    ADD CONSTRAINT "FK_6f8d32646cd388c667b27e5135d" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: skill_feat FK_70e17a91ef81449298f9de25c59; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_feat
    ADD CONSTRAINT "FK_70e17a91ef81449298f9de25c59" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: character FK_74a400a1643ce47965a8a231bbf; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_74a400a1643ce47965a8a231bbf" FOREIGN KEY ("clazzId") REFERENCES public.class(id);


--
-- Name: item FK_76ba3e80c7d03c62d171ae152ea; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT "FK_76ba3e80c7d03c62d171ae152ea" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: status_effect FK_774ac5e5c5f2ab7d71e50b60aea; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect
    ADD CONSTRAINT "FK_774ac5e5c5f2ab7d71e50b60aea" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: character FK_7b670580b08c1e3b25fc3658c90; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_7b670580b08c1e3b25fc3658c90" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: skill_feat FK_823046cfb4508af876f5094bf62; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_feat
    ADD CONSTRAINT "FK_823046cfb4508af876f5094bf62" FOREIGN KEY ("skillId") REFERENCES public.skill(id);


--
-- Name: character_skill_feat FK_839cc571d017c13889b0cae7e28; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill_feat
    ADD CONSTRAINT "FK_839cc571d017c13889b0cae7e28" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character_skill_feat FK_856b3c66e5ad740605a27c9ba9e; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill_feat
    ADD CONSTRAINT "FK_856b3c66e5ad740605a27c9ba9e" FOREIGN KEY ("featId") REFERENCES public.skill_feat(id);


--
-- Name: status_effect FK_906ceaa00830b7e540b68bc7d1d; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect
    ADD CONSTRAINT "FK_906ceaa00830b7e540b68bc7d1d" FOREIGN KEY ("universalModifierId") REFERENCES public.universal_modifier(id);


--
-- Name: equipment_skill_modifiers_skill_modifier FK_90b8d41223816f64c0d1e23c1f1; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_90b8d41223816f64c0d1e23c1f1" FOREIGN KEY ("skillModifierId") REFERENCES public.skill_modifier(id) ON DELETE CASCADE;


--
-- Name: character_stat FK_92bc9e28e14d2d03292e0d28a94; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat
    ADD CONSTRAINT "FK_92bc9e28e14d2d03292e0d28a94" FOREIGN KEY ("statId") REFERENCES public.stat(id);


--
-- Name: character_item FK_a533a7339e040463d5d2bfa7da6; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item
    ADD CONSTRAINT "FK_a533a7339e040463d5d2bfa7da6" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character FK_a7485d062b19695002b6175e8fb; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_a7485d062b19695002b6175e8fb" FOREIGN KEY ("raceId") REFERENCES public.race(id);


--
-- Name: character_item FK_a9ccbb5fe58c0fcc9be39bda7ea; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item
    ADD CONSTRAINT "FK_a9ccbb5fe58c0fcc9be39bda7ea" FOREIGN KEY ("itemId") REFERENCES public.item(id);


--
-- Name: status_effect_stat_modifiers_stat_modifier FK_b21f62444df75b2bf76d0eeea61; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_b21f62444df75b2bf76d0eeea61" FOREIGN KEY ("statModifierId") REFERENCES public.stat_modifier(id) ON DELETE CASCADE;


--
-- Name: equipment_stat_modifiers_stat_modifier FK_bb9c6775cea83d5c0decb498482; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_bb9c6775cea83d5c0decb498482" FOREIGN KEY ("statModifierId") REFERENCES public.stat_modifier(id) ON DELETE CASCADE;


--
-- Name: stat_modifier FK_d001450c9d60eaf9fc04fa53a1a; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat_modifier
    ADD CONSTRAINT "FK_d001450c9d60eaf9fc04fa53a1a" FOREIGN KEY ("statId") REFERENCES public.stat(id);


--
-- Name: character_skill FK_e12377dbce6154a7bcb2db70cbc; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill
    ADD CONSTRAINT "FK_e12377dbce6154a7bcb2db70cbc" FOREIGN KEY ("skillId") REFERENCES public.skill(id);


--
-- Name: skill_modifier FK_e559875de56112fb6ecd75b2fe9; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_modifier
    ADD CONSTRAINT "FK_e559875de56112fb6ecd75b2fe9" FOREIGN KEY ("skillId") REFERENCES public.skill(id);


--
-- Name: equipment_skill_modifiers_skill_modifier FK_e73e4d8a0f2ae9a1816968fdc43; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_e73e4d8a0f2ae9a1816968fdc43" FOREIGN KEY ("equipmentId") REFERENCES public.equipment(id) ON DELETE CASCADE;


--
-- Name: stat FK_f3c5b7a4bb65961c54dddcad16a; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat
    ADD CONSTRAINT "FK_f3c5b7a4bb65961c54dddcad16a" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: equipment FK_f87c9fb7bed46932a98eeeb0c67; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT "FK_f87c9fb7bed46932a98eeeb0c67" FOREIGN KEY ("universalModifierId") REFERENCES public.universal_modifier(id);


--
-- Name: equipment FK_fb5b513ae2040f78911353ae9a6; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT "FK_fb5b513ae2040f78911353ae9a6" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

