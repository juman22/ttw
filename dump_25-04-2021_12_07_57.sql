--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases (except postgres and template1)
--

DROP DATABASE typeormtest;




--
-- Drop roles
--

DROP ROLE typeormtest;


--
-- Roles
--

CREATE ROLE typeormtest;
ALTER ROLE typeormtest WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md580faed1ece11b640c123b659b168d113';






--
-- Databases
--

--
-- Database "template1" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

UPDATE pg_catalog.pg_database SET datistemplate = false WHERE datname = 'template1';
DROP DATABASE template1;
--
-- Name: template1; Type: DATABASE; Schema: -; Owner: typeormtest
--

CREATE DATABASE template1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE template1 OWNER TO typeormtest;

\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: typeormtest
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: template1; Type: DATABASE PROPERTIES; Schema: -; Owner: typeormtest
--

ALTER DATABASE template1 IS_TEMPLATE = true;


\connect template1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: ACL; Schema: -; Owner: typeormtest
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE postgres;
--
-- Name: postgres; Type: DATABASE; Schema: -; Owner: typeormtest
--

CREATE DATABASE postgres WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE postgres OWNER TO typeormtest;

\connect postgres

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: typeormtest
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- PostgreSQL database dump complete
--

--
-- Database "typeormtest" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1.pgdg100+1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: typeormtest; Type: DATABASE; Schema: -; Owner: typeormtest
--

CREATE DATABASE typeormtest WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE typeormtest OWNER TO typeormtest;

\connect typeormtest

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: character; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public."character" (
    id integer NOT NULL,
    name character varying NOT NULL,
    hp integer NOT NULL,
    "maxHp" integer NOT NULL,
    "endurantCharges" integer NOT NULL,
    "userId" integer,
    "settingId" integer,
    "raceId" integer,
    "clazzId" integer,
    notes text NOT NULL
);


ALTER TABLE public."character" OWNER TO typeormtest;

--
-- Name: character_effect; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_effect (
    id integer NOT NULL,
    "characterId" integer,
    "statusEffectId" integer
);


ALTER TABLE public.character_effect OWNER TO typeormtest;

--
-- Name: character_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_effect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_effect_id_seq OWNER TO typeormtest;

--
-- Name: character_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_effect_id_seq OWNED BY public.character_effect.id;


--
-- Name: character_equipment; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_equipment (
    id integer NOT NULL,
    "isEquipped" boolean NOT NULL,
    "characterId" integer,
    "equipmentId" integer
);


ALTER TABLE public.character_equipment OWNER TO typeormtest;

--
-- Name: character_equipment_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_equipment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_equipment_id_seq OWNER TO typeormtest;

--
-- Name: character_equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_equipment_id_seq OWNED BY public.character_equipment.id;


--
-- Name: character_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_id_seq OWNER TO typeormtest;

--
-- Name: character_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_id_seq OWNED BY public."character".id;


--
-- Name: character_item; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_item (
    id integer NOT NULL,
    quantity integer NOT NULL,
    "characterId" integer,
    "itemId" integer
);


ALTER TABLE public.character_item OWNER TO typeormtest;

--
-- Name: character_item_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_item_id_seq OWNER TO typeormtest;

--
-- Name: character_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_item_id_seq OWNED BY public.character_item.id;


--
-- Name: character_skill; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_skill (
    id integer NOT NULL,
    experience integer NOT NULL,
    "characterId" integer,
    "skillId" integer
);


ALTER TABLE public.character_skill OWNER TO typeormtest;

--
-- Name: character_skill_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_skill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_skill_id_seq OWNER TO typeormtest;

--
-- Name: character_skill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_skill_id_seq OWNED BY public.character_skill.id;


--
-- Name: character_stat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.character_stat (
    id integer NOT NULL,
    value integer NOT NULL,
    "characterId" integer,
    "statId" integer
);


ALTER TABLE public.character_stat OWNER TO typeormtest;

--
-- Name: character_stat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.character_stat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_stat_id_seq OWNER TO typeormtest;

--
-- Name: character_stat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.character_stat_id_seq OWNED BY public.character_stat.id;


--
-- Name: class; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.class (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer
);


ALTER TABLE public.class OWNER TO typeormtest;

--
-- Name: class_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.class_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.class_id_seq OWNER TO typeormtest;

--
-- Name: class_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.class_id_seq OWNED BY public.class.id;


--
-- Name: equipment; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.equipment (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "universalModifierId" integer,
    notes character varying(200) NOT NULL
);


ALTER TABLE public.equipment OWNER TO typeormtest;

--
-- Name: equipment_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.equipment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipment_id_seq OWNER TO typeormtest;

--
-- Name: equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.equipment_id_seq OWNED BY public.equipment.id;


--
-- Name: equipment_skill_modifiers_skill_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.equipment_skill_modifiers_skill_modifier (
    "equipmentId" integer NOT NULL,
    "skillModifierId" integer NOT NULL
);


ALTER TABLE public.equipment_skill_modifiers_skill_modifier OWNER TO typeormtest;

--
-- Name: equipment_stat_modifiers_stat_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.equipment_stat_modifiers_stat_modifier (
    "equipmentId" integer NOT NULL,
    "statModifierId" integer NOT NULL
);


ALTER TABLE public.equipment_stat_modifiers_stat_modifier OWNER TO typeormtest;

--
-- Name: item; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.item (
    id integer NOT NULL,
    name character varying NOT NULL,
    notes character varying(200) NOT NULL,
    "settingId" integer
);


ALTER TABLE public.item OWNER TO typeormtest;

--
-- Name: item_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.item_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_id_seq OWNER TO typeormtest;

--
-- Name: item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.item_id_seq OWNED BY public.item.id;


--
-- Name: race; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.race (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer
);


ALTER TABLE public.race OWNER TO typeormtest;

--
-- Name: race_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.race_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.race_id_seq OWNER TO typeormtest;

--
-- Name: race_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.race_id_seq OWNED BY public.race.id;


--
-- Name: setting; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.setting (
    id integer NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL
);


ALTER TABLE public.setting OWNER TO typeormtest;

--
-- Name: setting_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setting_id_seq OWNER TO typeormtest;

--
-- Name: setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.setting_id_seq OWNED BY public.setting.id;


--
-- Name: skill; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.skill (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "statId" integer
);


ALTER TABLE public.skill OWNER TO typeormtest;

--
-- Name: skill_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.skill_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_id_seq OWNER TO typeormtest;

--
-- Name: skill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.skill_id_seq OWNED BY public.skill.id;


--
-- Name: skill_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.skill_modifier (
    id integer NOT NULL,
    value integer NOT NULL,
    "skillId" integer
);


ALTER TABLE public.skill_modifier OWNER TO typeormtest;

--
-- Name: skill_modifier_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.skill_modifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.skill_modifier_id_seq OWNER TO typeormtest;

--
-- Name: skill_modifier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.skill_modifier_id_seq OWNED BY public.skill_modifier.id;


--
-- Name: stat; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.stat (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer
);


ALTER TABLE public.stat OWNER TO typeormtest;

--
-- Name: stat_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.stat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stat_id_seq OWNER TO typeormtest;

--
-- Name: stat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.stat_id_seq OWNED BY public.stat.id;


--
-- Name: stat_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.stat_modifier (
    id integer NOT NULL,
    value integer NOT NULL,
    "statId" integer
);


ALTER TABLE public.stat_modifier OWNER TO typeormtest;

--
-- Name: stat_modifier_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.stat_modifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stat_modifier_id_seq OWNER TO typeormtest;

--
-- Name: stat_modifier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.stat_modifier_id_seq OWNED BY public.stat_modifier.id;


--
-- Name: status_effect; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.status_effect (
    id integer NOT NULL,
    name character varying NOT NULL,
    "settingId" integer,
    "universalModifierId" integer
);


ALTER TABLE public.status_effect OWNER TO typeormtest;

--
-- Name: status_effect_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.status_effect_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_effect_id_seq OWNER TO typeormtest;

--
-- Name: status_effect_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.status_effect_id_seq OWNED BY public.status_effect.id;


--
-- Name: status_effect_skill_modifiers_skill_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.status_effect_skill_modifiers_skill_modifier (
    "statusEffectId" integer NOT NULL,
    "skillModifierId" integer NOT NULL
);


ALTER TABLE public.status_effect_skill_modifiers_skill_modifier OWNER TO typeormtest;

--
-- Name: status_effect_stat_modifiers_stat_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.status_effect_stat_modifiers_stat_modifier (
    "statusEffectId" integer NOT NULL,
    "statModifierId" integer NOT NULL
);


ALTER TABLE public.status_effect_stat_modifiers_stat_modifier OWNER TO typeormtest;

--
-- Name: universal_modifier; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public.universal_modifier (
    id integer NOT NULL,
    hp integer NOT NULL,
    "armorClass" integer NOT NULL,
    "movementSpeed" integer NOT NULL,
    overdose integer NOT NULL,
    universal integer NOT NULL
);


ALTER TABLE public.universal_modifier OWNER TO typeormtest;

--
-- Name: universal_modifier_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.universal_modifier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.universal_modifier_id_seq OWNER TO typeormtest;

--
-- Name: universal_modifier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.universal_modifier_id_seq OWNED BY public.universal_modifier.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: typeormtest
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying NOT NULL,
    uid character varying NOT NULL
);


ALTER TABLE public."user" OWNER TO typeormtest;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: typeormtest
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO typeormtest;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: typeormtest
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: character id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character" ALTER COLUMN id SET DEFAULT nextval('public.character_id_seq'::regclass);


--
-- Name: character_effect id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect ALTER COLUMN id SET DEFAULT nextval('public.character_effect_id_seq'::regclass);


--
-- Name: character_equipment id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment ALTER COLUMN id SET DEFAULT nextval('public.character_equipment_id_seq'::regclass);


--
-- Name: character_item id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item ALTER COLUMN id SET DEFAULT nextval('public.character_item_id_seq'::regclass);


--
-- Name: character_skill id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill ALTER COLUMN id SET DEFAULT nextval('public.character_skill_id_seq'::regclass);


--
-- Name: character_stat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat ALTER COLUMN id SET DEFAULT nextval('public.character_stat_id_seq'::regclass);


--
-- Name: class id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class ALTER COLUMN id SET DEFAULT nextval('public.class_id_seq'::regclass);


--
-- Name: equipment id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment ALTER COLUMN id SET DEFAULT nextval('public.equipment_id_seq'::regclass);


--
-- Name: item id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.item ALTER COLUMN id SET DEFAULT nextval('public.item_id_seq'::regclass);


--
-- Name: race id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.race ALTER COLUMN id SET DEFAULT nextval('public.race_id_seq'::regclass);


--
-- Name: setting id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.setting ALTER COLUMN id SET DEFAULT nextval('public.setting_id_seq'::regclass);


--
-- Name: skill id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill ALTER COLUMN id SET DEFAULT nextval('public.skill_id_seq'::regclass);


--
-- Name: skill_modifier id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_modifier ALTER COLUMN id SET DEFAULT nextval('public.skill_modifier_id_seq'::regclass);


--
-- Name: stat id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat ALTER COLUMN id SET DEFAULT nextval('public.stat_id_seq'::regclass);


--
-- Name: stat_modifier id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat_modifier ALTER COLUMN id SET DEFAULT nextval('public.stat_modifier_id_seq'::regclass);


--
-- Name: status_effect id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect ALTER COLUMN id SET DEFAULT nextval('public.status_effect_id_seq'::regclass);


--
-- Name: universal_modifier id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.universal_modifier ALTER COLUMN id SET DEFAULT nextval('public.universal_modifier_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: character; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public."character" (id, name, hp, "maxHp", "endurantCharges", "userId", "settingId", "raceId", "clazzId", notes) FROM stdin;
2	Conqure	10	10	0	3	1	1	4	**Money**\n$125\n\n**Inspirations**\n-1d4 x 3\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n
5	Bo of Bo Industries TM	12	12	1	6	1	4	3	**Money**\n$125\n\n**Inspirations**\n-1d4 x 3\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n
1	Test	3	5	0	2	1	1	1	**Money**\n$125\n\n**Inspirations**\n-1d4 x 3\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n
4	Terran "Dos Cadenas" Allen	6	12	1	4	1	3	4	**Money**\n$275\n\n**Inspirations**\n-1x 1d8\n\n**Master Skills**\nRemote Start: You’ve driven your ship enough to pilot it well… blind! As an action you can pilot your ship remotely.\n\n\n**Inhumanity Score**\n- 4\n\nLast Legs\n--While ‘downed’ you may continue acting as if you were alive for 1 + IS turns.Taking damage in this state is considered critical damage. Falling below negative your maximum health is death.\n\nMechanical Internals\n--You may add your IS to overdose checks.\n\nCaloric Tolerance\n--If you possess an IS of 4 you no longer take daily damage from being EMPTY on food.\n\nBody Slam\n--When making a melee strike you can attempt to make an additional attack using your mechanical parts. Make a regular melee attack roll and add your IS. If the attack connects your opponent takes 1d4+IS to 1d6+IS damage.
3	Terran "Dos Cadenas" Allen	12	12	1	4	1	3	4	USE OTHER CHARACTER\n
7	Drake fowl	6	11	1	5	1	3	2	**Money**\n$70\n\n**Inspirations**\n-1d4 x 3\n-1d8 x 1\n\n**Master Skills**\n- Move Aside!\nFlex your authority and exploit the ignorant populace. You can move past enemies without incurring attacks of opportunity. You also can use your surgery skill instead of persuasion in a crisis or medical context.\n\n- Hold This\nTime apart makes the heart grow fonder. Deposit your weapon inside your enemy and leave it there on a successful attack roll. It deals half damage to them every turn it is lodged inside them. An enemy must make a melee weapon save with a DC of the attack roll you made to remove it, and can repeat that save every turn.\n\n- Detox Expert\nYou’ve invested in a portable dialysis machine capable of filtering even the most toxic of drugs from users. First aid checks can now be used to decrease toxicity. A successful roll (>10) will remove 3 toxicity. A critical success (>15) will remove 5 toxicity.\n
6	Sunny	9	12	2	7	1	5	1	**Current Status Effects**\n- +1 to all rolls (Apple Pie)\n\n**Actions**\n- 1 action\n- 1 reaction\n- 1 consumable\n\n**SRS**\n- Level 2\n\n**Money**\n- $4\n\n**Inspirations**\n- 1d10\n- 2d8\n- 2d4\n- 1d6\n\n**Master Skills**\n\n- DEX Melee Weapon\n  - Parlor Tricks\nYour weapon of choice is an extension of yourself. Spin it, flip it and toss it. What’s a little danger for some street cred. You must make your attack roll twice, but if you succeed you stun the enemy.\n\n- STEAL\n  - Concealed Carry\nRoll a steal to use a weapon without being detected. Bystanders may still realize that an attack has been made, but will be unable to connect you to it within reason.\n\n- LOCKPICK\n  - Mystery of the White Whale\nThere exists a lock in the world, unopened for centuries. No one knows its whereabouts, but its existence taunts lockpickers the world over. Luckily for you.. You just got a lead.\n\n- SNEAK\n  - Shadow Strike\nDangers unseen are twice as deadly. Attack rolls made while sneaking are performed at advantage.\n\n- PERSUASION\n  - Monologue\nAs an action in combat, you can attempt to engross an enemy with a powerful monologue. If they fail a persuasion contest with you they are stunned for 1d4 turns. They are allowed to reattempt the contest every stunned turn. You are busy monologuing throughout the stun. While you can take other non-verbal actions, you are not capable of communicating to anyone else while stunning a target this way. This effect also requires line of sight.
\.


--
-- Data for Name: character_effect; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_effect (id, "characterId", "statusEffectId") FROM stdin;
\.


--
-- Data for Name: character_equipment; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_equipment (id, "isEquipped", "characterId", "equipmentId") FROM stdin;
2	f	2	1
4	t	4	3
3	t	4	4
5	t	4	5
6	t	4	6
1	t	4	1
7	t	7	7
8	t	7	10
11	t	5	9
10	t	5	8
14	t	6	14
15	t	6	15
16	t	6	16
17	t	6	17
19	t	6	19
22	t	5	20
24	t	6	22
25	t	5	23
9	f	5	7
26	t	7	20
30	t	4	25
54	t	6	32
45	f	1	1
48	f	1	14
59	f	1	16
\.


--
-- Data for Name: character_item; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_item (id, quantity, "characterId", "itemId") FROM stdin;
5	1	7	3
6	1	7	4
8	1	7	8
10	1	6	10
11	2	6	7
12	2	6	6
13	10	6	9
14	1	6	13
16	1	6	14
17	1	6	15
21	1	4	18
22	1	4	14
23	1	4	16
24	1	4	5
25	2	4	6
26	2	4	7
27	1	4	21
28	1	4	22
29	1	1	7
31	1	7	23
32	1	7	12
33	1	7	14
34	1	7	16
36	1	7	24
37	1	7	25
35	4	7	34
38	2	7	26
39	3	7	19
40	5	7	20
41	1	7	22
43	1	7	29
45	1	7	31
44	2	7	30
46	1	7	32
47	3	7	35
48	1	7	33
7	1	6	5
42	1	7	7
49	1	1	8
50	1	1	14
52	1	1	16
\.


--
-- Data for Name: character_skill; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_skill (id, experience, "characterId", "skillId") FROM stdin;
10	0	1	17
11	0	1	4
12	0	1	6
15	0	1	18
17	0	1	20
20	0	1	22
22	0	1	10
24	0	1	24
151	32	7	1
51	10	3	1
152	0	7	11
52	20	3	11
53	0	3	12
54	0	3	13
55	202	3	14
56	26	3	2
57	449	3	15
58	62	3	7
59	82	3	5
60	26	3	6
61	0	3	4
62	295	3	16
63	40	3	3
64	40	3	10
65	0	3	8
66	20	3	9
67	0	3	17
68	0	3	19
69	0	3	18
70	202	3	20
71	449	3	21
72	0	3	22
73	15	3	23
74	0	3	24
75	0	3	25
77	26	4	2
33	4	2	16
35	70	2	5
36	0	2	8
78	40	4	3
79	0	4	4
81	26	4	6
82	62	4	7
83	0	4	8
84	40	4	10
85	20	4	9
86	20	4	11
87	0	4	12
88	0	4	13
89	202	4	14
92	0	4	17
93	0	4	18
94	0	4	19
95	202	4	20
96	449	4	21
97	0	4	22
98	15	4	23
99	0	4	24
100	0	4	25
26	1469	2	1
76	23	4	1
153	0	7	12
154	0	7	13
155	0	7	14
157	0	7	17
159	0	7	18
161	0	7	5
90	647	4	15
27	34	2	11
28	14	2	12
29	14	2	13
30	14	2	14
31	14	2	15
32	62	2	2
34	10	2	9
37	10	2	7
38	147	2	3
39	15	2	18
40	82	2	4
41	10	2	6
42	15	2	17
102	0	5	11
43	155	2	10
47	15	2	22
45	26	2	20
44	15	2	19
46	15	2	21
48	13	2	23
49	13	2	24
50	13	2	25
104	0	5	13
105	0	5	14
106	0	5	15
107	14	5	6
111	80	5	5
112	0	5	8
113	0	5	3
114	0	5	17
115	0	5	4
116	0	5	18
117	0	5	9
118	0	5	22
119	0	5	19
120	0	5	20
121	160	5	23
122	0	5	21
123	0	5	24
125	0	5	10
162	0	7	6
126	36	6	1
127	0	6	11
128	0	6	12
129	0	6	13
130	0	6	14
131	0	6	15
133	0	6	2
134	0	6	17
135	0	6	3
136	0	6	18
138	200	6	9
140	0	6	4
143	56	6	19
144	19	6	5
145	0	6	20
146	0	6	21
147	14	6	22
148	271	6	23
149	0	6	24
150	0	6	25
164	0	7	4
166	0	7	9
167	0	7	20
168	233	7	21
169	0	7	8
170	0	7	10
171	506	7	22
172	0	7	23
173	0	7	24
174	0	7	25
101	104	5	1
18	15	1	8
3	15	1	12
165	42	7	7
163	551	7	19
142	317	6	8
80	105	4	5
124	392	5	25
175	0	1	26
176	0	2	26
177	0	3	26
178	0	4	26
179	0	5	26
180	0	6	26
181	0	7	26
8	15	1	5
132	28	6	16
5	288	1	14
21	22	1	23
160	547	7	3
9	606	1	2
6	288	1	15
25	22	1	25
4	305	1	13
1	275	1	1
13	94	1	3
23	90	1	9
2	273	1	11
109	12	5	2
14	300	1	7
156	138	7	16
7	24	1	16
139	76	6	6
91	371	4	16
108	468	5	16
158	46	7	2
19	141	1	21
137	340	6	10
110	43	5	7
16	113	1	19
141	540	6	7
103	852	5	12
\.


--
-- Data for Name: character_stat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.character_stat (id, value, "characterId", "statId") FROM stdin;
1	13	1	1
2	15	1	2
3	7	1	3
4	14	1	4
5	15	1	5
6	11	1	7
7	12	1	6
8	12	2	1
9	10	2	2
10	6	2	3
11	19	2	4
12	10	2	5
13	11	2	6
14	9	2	7
15	13	3	1
16	21	3	2
17	15	3	3
18	13	3	4
19	22	3	5
20	19	3	6
21	15	3	7
22	13	4	1
23	21	4	2
24	15	4	3
25	13	4	4
26	22	4	5
27	19	4	6
28	15	4	7
29	12	5	1
30	14	5	2
31	17	5	3
32	8	5	4
33	17	5	5
34	16	5	6
35	21	5	7
36	12	6	1
37	19	6	2
38	15	6	3
39	12	6	4
40	14	6	5
41	14	6	6
42	20	6	7
43	16	7	1
44	14	7	2
45	14	7	3
46	16	7	4
47	16	7	5
48	23	7	6
49	7	7	7
\.


--
-- Data for Name: class; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.class (id, name, "settingId") FROM stdin;
1	Symbient	1
2	Juicer	1
3	Cipher	1
4	Machinist	1
\.


--
-- Data for Name: equipment; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.equipment (id, name, "settingId", "universalModifierId", notes) FROM stdin;
1	Kevlite	1	1	
6	Grasping Hands	1	6	
7	Leather Jacket	1	7	
9	AI Combat Visor	1	9	
10	Bonesaw	1	10	
14	Spectral Armor	1	14	
15	HoloMask	1	15	
17	Deflection Bracers	1	17	
18	Charged Circlet	1	18	
19	Chat Ring	1	19	
21	Child's Fur Lined Jacket	1	21	
22	Electrolysis Band	1	22	
23	Electromagnetic Coil	1	23	
24	Boombox Boots	1	24	
25	Threaded Band	1	25	
20	Jogging Pants	1	20	
3	Hydraulic Stabilizers	1	3	Kick\n1d10 physical damage\n1d10 siege damage\n5 turn cooldown\n
4	Fusion Core	1	4	Nuke Ignition\n2d6 fire damage\n1d6 siege damage\n1 day cooldown
5	Seeker Rifle	1	5	1d12 physical damage, 1d8 critical fire damage\n50ft range
8	Microwave Toolkit	1	8	1d6 fire damage\n1d8 fire critical damage
11	Boomerang Blade	1	11	1d6 physical damage\n5ft range, 30ft throw range
16	Lightning Glove	1	16	Melee Attack\n1d6 physical damage\n1d6 electrical critical damage.\nRanged Attack\n1d8 electrical damage\n1d8 (2-spread) electrical critical damage\n15ft
32	Taser Blade	1	42	DC 10 return catch, 1d4 physical damage on failure. \nCritical damage can be distributed to two enemies within range.\n+100% critical chance (19,20).\n\n\n
\.


--
-- Data for Name: equipment_skill_modifiers_skill_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.equipment_skill_modifiers_skill_modifier ("equipmentId", "skillModifierId") FROM stdin;
6	1
6	2
8	3
9	4
10	5
16	6
22	7
\.


--
-- Data for Name: equipment_stat_modifiers_stat_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.equipment_stat_modifiers_stat_modifier ("equipmentId", "statModifierId") FROM stdin;
1	2
3	6
3	7
4	9
4	11
7	24
9	26
20	36
21	38
22	39
23	40
24	41
25	42
\.


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.item (id, name, notes, "settingId") FROM stdin;
3	Gen Spice	A dose of Orange sparkly liquid that gives a burst of energy	1
4	Eye Opener	A dose of pale blue liquid, aids brain function for a short time	1
5	Incinerator Grenade	15 ft Radius\n1d6 fire damage\n	1
6	Emergency Kits	1d4 healing	1
7	Specialist's Kit	2d4 healing	1
8	Flex Juice	A dose of glowing red liquid, gives a tremendous boost to physical capabilities. *WARNING*: overdose can be fatal!	1
9	Generic Pre-Pack	1x food	1
10	Deckard's Brain	a dog brain in a jar	1
12	Exotic Bar: Guava Berry	+3 Strength\n+1 Food	1
13	Dine-in Ready Meal	x2 Food\n+2 Strength	1
14	Exotic Bar: Pomenango	+3 Charisma\n+1 Food	1
15	Exotic Bar: CocoNutty	+2 CON\n+2 HP\nx1 Food	1
16	Exotic Bar: Chocoholic	-1 Dexterity\n+2 Food	1
19	Resistance Gel	+5 toxicity\nAs a reaction heal back half of received damage\n1 Turn\nOverdose Effects\n-1 HP\nMax Per Day. 1	1
20	Scalpels	1d4 physical dmg	1
21	Morphine Dart	1d4 heal\n30 ft range\n	1
22	Defibrillator	Use as a reaction on a fatal hit to stay conscious at 1hp on a successful first aid check.	1
23	Uzi	2d4 physical damage, 1d8 physical critical damage\n20ft range	1
24	Day Dream	I FEEL FANTASTIC\nAlso causes terrible hangover	1
25	Ash Virus	vials of mystery drug	1
27	Defibrilator	first aid pass reaction on fatal hit to come back at 1hp	1
28	Specialist Kit	2d4 heal consumable	1
29	Roid-it	+4 toxicity\nRe-roll an ability check or attack.\nOverdose Effects\n1d10 poison damage	1
30	Speed Gum	+2 toxicity\n+20 ft of movement\nOverdose Effects\nYou gain a limp\n1d4 poison damage	1
31	Desert Spice	+3 toxicity\nYou gain the ability to spit fire.\n1d4 fire damage\n2 Turns\nOverdose Effects\n1d6 fire damage	1
32	Cretid Venom	+3 Toxicity\n+2 Strength\n-3 Intelligence\n3 Turns\nOverdose Effects\n-2 HP\n-5 Intelligence\nMax Per Day. 0	1
33	Brain Rot	+3 Toxicity\n+2 Strength\n+1 intelligence \n+1 Wisdom\n3 Turns\nOverdose Effects\n-9 Intelligence\n-2 HP\nMax per day: 0	1
34	Mystery Serum	Cures the ash virus	1
35	Mystery Numbing liquid	Strange medical numbing liquid used to preserve dog brains	1
26	Neo-Coke	+1 Intelligence\n+1 food	1
18	Equality Apple Pie	+1 to all rolls until expiration	1
\.


--
-- Data for Name: race; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.race (id, name, "settingId") FROM stdin;
1	Old Worlder	1
2	Peasant	1
3	Tecchi	1
4	Noble	1
5	Patrician	1
\.


--
-- Data for Name: setting; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.setting (id, name, description) FROM stdin;
1	Tronix	Cyberpunk
\.


--
-- Data for Name: skill; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.skill (id, name, "settingId", "statId") FROM stdin;
1	Heavy Guns	1	1
4	Throwing	1	1
5	Precision Guns	1	2
7	Sneak	1	2
8	Lockpick	1	2
9	Steal	1	2
11	Energy Weapons	1	5
12	Hacking	1	5
13	Traps	1	5
16	Personal Piloting	1	6
17	Freighter Piloting	1	6
18	Diagnostics	1	6
19	First Aid	1	6
22	Surgery	1	6
23	Persuasion	1	7
24	Intimidation	1	7
25	Trade	1	7
3	Melee Weapons (Str)	1	1
2	Unarmed (Str)	1	1
6	Unarmed (Dex)	1	2
10	Melee Weapons (Dex)	1	2
15	Repair (Int)	1	5
21	Repair (Wis)	1	6
14	Engineering (Int)	1	5
20	Engineering (Wis)	1	6
26	Searching	1	6
\.


--
-- Data for Name: skill_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.skill_modifier (id, value, "skillId") FROM stdin;
1	3	8
2	7	9
3	1	12
4	1	12
5	2	22
6	1	24
7	2	19
\.


--
-- Data for Name: stat; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.stat (id, name, "settingId") FROM stdin;
1	Strength	1
2	Dexterity	1
3	Constitution	1
4	Endurance	1
5	Intelligence	1
6	Wisdom	1
7	Charisma	1
\.


--
-- Data for Name: stat_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.stat_modifier (id, value, "statId") FROM stdin;
2	-1	7
4	-1	7
6	2	2
7	2	4
9	1	1
11	1	3
24	2	7
26	1	5
28	3	1
29	3	2
30	-10	1
32	-10	1
34	1	1
36	1	4
38	1	7
39	1	3
40	1	2
41	1	2
42	1	2
43	2	1
44	1	5
45	1	6
\.


--
-- Data for Name: status_effect; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.status_effect (id, name, "settingId", "universalModifierId") FROM stdin;
1	Brain Rot	1	26
\.


--
-- Data for Name: status_effect_skill_modifiers_skill_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.status_effect_skill_modifiers_skill_modifier ("statusEffectId", "skillModifierId") FROM stdin;
\.


--
-- Data for Name: status_effect_stat_modifiers_stat_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.status_effect_stat_modifiers_stat_modifier ("statusEffectId", "statModifierId") FROM stdin;
1	43
1	44
1	45
\.


--
-- Data for Name: universal_modifier; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public.universal_modifier (id, hp, "armorClass", "movementSpeed", overdose, universal) FROM stdin;
1	0	2	0	0	0
2	0	2	0	0	0
3	0	0	0	0	0
4	0	0	0	0	0
5	0	0	0	0	0
6	0	0	0	0	0
7	0	1	0	0	0
8	0	0	0	0	0
9	0	0	0	0	0
10	0	0	0	0	0
11	0	0	0	0	0
12	0	0	0	0	0
13	0	0	0	0	0
14	0	3	0	0	0
15	0	0	0	0	0
16	0	1	0	0	0
17	0	0	0	0	0
18	0	0	0	0	0
19	0	0	0	0	0
20	0	1	0	0	0
21	0	0	0	0	0
22	0	0	0	0	0
23	0	2	0	0	0
24	0	1	0	0	0
25	0	2	0	0	0
26	0	0	0	0	0
27	0	0	0	0	0
28	0	0	0	0	0
29	1	0	0	0	0
30	0	0	0	0	0
31	0	0	0	0	15
32	0	0	0	0	7
33	0	0	0	0	0
34	0	0	0	0	0
35	0	0	0	0	0
36	0	0	0	0	0
37	0	0	0	0	0
38	0	0	0	0	0
39	0	0	0	0	0
40	0	0	0	0	0
41	0	0	0	0	0
42	0	0	0	0	0
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: typeormtest
--

COPY public."user" (id, email, uid) FROM stdin;
1	Jeffy@amazon.com	Zv1NPmF8jjXJzkOZs0ceL4gfyPT2
2	justinrwalters95@gmail.com	RM03e7cUsZRO4Ow9I4JVl0CLjAj1
3	jakewaltersss@gmail.com	yWIjNl4jGah4Gw1yVgw16Szn4DT2
4	treyh8794@hotmail.com	iloC77IMhVQt38GknVcKBBA1qbk2
5	jakewoodall1120@gmail.com	AREnPW13fFQwldg7xqQSP62o2Nl2
6	bhunter2394@gmail.com	Qi7G8d3AevXmxp1GS90HUjE1UW13
7	hjwalters21@gmail.com	PPLcLUTEGINTObiV8DXO2KU0FGA3
8	Yasmeen76@gmail.com	3Lmb4MbL66Szq8XxRDWzMRy3tj43
9	man@gmail.com	HbFPOSSWzgadkDeeVTjidR7P28n1
10	datBoi@datboiserver.com	k1JE4AUnGPMRkFAJrCFGzxRH3Xz2
\.


--
-- Name: character_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_effect_id_seq', 9, true);


--
-- Name: character_equipment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_equipment_id_seq', 59, true);


--
-- Name: character_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_id_seq', 7, true);


--
-- Name: character_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_item_id_seq', 52, true);


--
-- Name: character_skill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_skill_id_seq', 181, true);


--
-- Name: character_stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.character_stat_id_seq', 49, true);


--
-- Name: class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.class_id_seq', 4, true);


--
-- Name: equipment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.equipment_id_seq', 32, true);


--
-- Name: item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.item_id_seq', 35, true);


--
-- Name: race_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.race_id_seq', 5, true);


--
-- Name: setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.setting_id_seq', 1, true);


--
-- Name: skill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.skill_id_seq', 26, true);


--
-- Name: skill_modifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.skill_modifier_id_seq', 7, true);


--
-- Name: stat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.stat_id_seq', 7, true);


--
-- Name: stat_modifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.stat_modifier_id_seq', 46, true);


--
-- Name: status_effect_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.status_effect_id_seq', 2, true);


--
-- Name: universal_modifier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.universal_modifier_id_seq', 42, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: typeormtest
--

SELECT pg_catalog.setval('public.user_id_seq', 10, true);


--
-- Name: equipment PK_0722e1b9d6eb19f5874c1678740; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT "PK_0722e1b9d6eb19f5874c1678740" PRIMARY KEY (id);


--
-- Name: universal_modifier PK_078b14b5aa1511cc02850035b1b; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.universal_modifier
    ADD CONSTRAINT "PK_078b14b5aa1511cc02850035b1b" PRIMARY KEY (id);


--
-- Name: class PK_0b9024d21bdfba8b1bd1c300eae; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class
    ADD CONSTRAINT "PK_0b9024d21bdfba8b1bd1c300eae" PRIMARY KEY (id);


--
-- Name: stat PK_132de903d366f4c06cd586c43c0; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat
    ADD CONSTRAINT "PK_132de903d366f4c06cd586c43c0" PRIMARY KEY (id);


--
-- Name: skill_modifier PK_360bb661107c161b70adaa377d2; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_modifier
    ADD CONSTRAINT "PK_360bb661107c161b70adaa377d2" PRIMARY KEY (id);


--
-- Name: status_effect_skill_modifiers_skill_modifier PK_3b5e6bdbf152b98fbf8450e481a; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_skill_modifiers_skill_modifier
    ADD CONSTRAINT "PK_3b5e6bdbf152b98fbf8450e481a" PRIMARY KEY ("statusEffectId", "skillModifierId");


--
-- Name: character_equipment PK_4afeeea248417ce2474b06820d9; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment
    ADD CONSTRAINT "PK_4afeeea248417ce2474b06820d9" PRIMARY KEY (id);


--
-- Name: character_effect PK_4fabba72f75951da75ace73c5bf; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect
    ADD CONSTRAINT "PK_4fabba72f75951da75ace73c5bf" PRIMARY KEY (id);


--
-- Name: character PK_6c4aec48c564968be15078b8ae5; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "PK_6c4aec48c564968be15078b8ae5" PRIMARY KEY (id);


--
-- Name: character_stat PK_7a7db5bbafa5832f6b8ee8feae7; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat
    ADD CONSTRAINT "PK_7a7db5bbafa5832f6b8ee8feae7" PRIMARY KEY (id);


--
-- Name: character_skill PK_835ce87665bea249a62f1a706c1; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill
    ADD CONSTRAINT "PK_835ce87665bea249a62f1a706c1" PRIMARY KEY (id);


--
-- Name: status_effect_stat_modifiers_stat_modifier PK_8cfa38a4574b63412e1afaaeda1; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_stat_modifiers_stat_modifier
    ADD CONSTRAINT "PK_8cfa38a4574b63412e1afaaeda1" PRIMARY KEY ("statusEffectId", "statModifierId");


--
-- Name: status_effect PK_9156e6e7704d054b364793ec3d4; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect
    ADD CONSTRAINT "PK_9156e6e7704d054b364793ec3d4" PRIMARY KEY (id);


--
-- Name: stat_modifier PK_96c2a5228fde4bc5ace781171d6; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat_modifier
    ADD CONSTRAINT "PK_96c2a5228fde4bc5ace781171d6" PRIMARY KEY (id);


--
-- Name: skill PK_a0d33334424e64fb78dc3ce7196; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT "PK_a0d33334424e64fb78dc3ce7196" PRIMARY KEY (id);


--
-- Name: race PK_a3068b184130d87a20e516045bb; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.race
    ADD CONSTRAINT "PK_a3068b184130d87a20e516045bb" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: item PK_d3c0c71f23e7adcf952a1d13423; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT "PK_d3c0c71f23e7adcf952a1d13423" PRIMARY KEY (id);


--
-- Name: equipment_skill_modifiers_skill_modifier PK_e13eda2e618db444b68f8e6f947; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_skill_modifiers_skill_modifier
    ADD CONSTRAINT "PK_e13eda2e618db444b68f8e6f947" PRIMARY KEY ("equipmentId", "skillModifierId");


--
-- Name: character_item PK_e4a897636d3db088f47702415a5; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item
    ADD CONSTRAINT "PK_e4a897636d3db088f47702415a5" PRIMARY KEY (id);


--
-- Name: equipment_stat_modifiers_stat_modifier PK_f4c6ab8bafa79d50f0e49545d07; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_stat_modifiers_stat_modifier
    ADD CONSTRAINT "PK_f4c6ab8bafa79d50f0e49545d07" PRIMARY KEY ("equipmentId", "statModifierId");


--
-- Name: setting PK_fcb21187dc6094e24a48f677bed; Type: CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.setting
    ADD CONSTRAINT "PK_fcb21187dc6094e24a48f677bed" PRIMARY KEY (id);


--
-- Name: IDX_0fb00fdc0806db73fa82e788c4; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_0fb00fdc0806db73fa82e788c4" ON public.status_effect_skill_modifiers_skill_modifier USING btree ("statusEffectId");


--
-- Name: IDX_459e9d51fc50c2d2c63e0d2fb2; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_459e9d51fc50c2d2c63e0d2fb2" ON public.status_effect_stat_modifiers_stat_modifier USING btree ("statusEffectId");


--
-- Name: IDX_5be50d8fcc7a8ae57732aabd93; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_5be50d8fcc7a8ae57732aabd93" ON public.status_effect_skill_modifiers_skill_modifier USING btree ("skillModifierId");


--
-- Name: IDX_5d6dbff6be99f1f751ddd25bdb; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_5d6dbff6be99f1f751ddd25bdb" ON public.equipment_stat_modifiers_stat_modifier USING btree ("equipmentId");


--
-- Name: IDX_90b8d41223816f64c0d1e23c1f; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_90b8d41223816f64c0d1e23c1f" ON public.equipment_skill_modifiers_skill_modifier USING btree ("skillModifierId");


--
-- Name: IDX_b21f62444df75b2bf76d0eeea6; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_b21f62444df75b2bf76d0eeea6" ON public.status_effect_stat_modifiers_stat_modifier USING btree ("statModifierId");


--
-- Name: IDX_bb9c6775cea83d5c0decb49848; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_bb9c6775cea83d5c0decb49848" ON public.equipment_stat_modifiers_stat_modifier USING btree ("statModifierId");


--
-- Name: IDX_e73e4d8a0f2ae9a1816968fdc4; Type: INDEX; Schema: public; Owner: typeormtest
--

CREATE INDEX "IDX_e73e4d8a0f2ae9a1816968fdc4" ON public.equipment_skill_modifiers_skill_modifier USING btree ("equipmentId");


--
-- Name: character FK_04c2fb52adfa5265763de8c4464; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_04c2fb52adfa5265763de8c4464" FOREIGN KEY ("userId") REFERENCES public."user"(id);


--
-- Name: character_equipment FK_0f0b9eee1410967891a80a64d24; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment
    ADD CONSTRAINT "FK_0f0b9eee1410967891a80a64d24" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: status_effect_skill_modifiers_skill_modifier FK_0fb00fdc0806db73fa82e788c44; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_0fb00fdc0806db73fa82e788c44" FOREIGN KEY ("statusEffectId") REFERENCES public.status_effect(id) ON DELETE CASCADE;


--
-- Name: skill FK_326ef0a6800b4254fda64ca792d; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT "FK_326ef0a6800b4254fda64ca792d" FOREIGN KEY ("statId") REFERENCES public.stat(id);


--
-- Name: class FK_37717e4f894378dfd3105dd62ff; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.class
    ADD CONSTRAINT "FK_37717e4f894378dfd3105dd62ff" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: character_stat FK_3829d539757783ad28f25c74c97; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat
    ADD CONSTRAINT "FK_3829d539757783ad28f25c74c97" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character_skill FK_39dcbdc540ee9ff68c7e39bcbe4; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill
    ADD CONSTRAINT "FK_39dcbdc540ee9ff68c7e39bcbe4" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character_equipment FK_406f5846e5afe5c32c03a94e604; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_equipment
    ADD CONSTRAINT "FK_406f5846e5afe5c32c03a94e604" FOREIGN KEY ("equipmentId") REFERENCES public.equipment(id);


--
-- Name: status_effect_stat_modifiers_stat_modifier FK_459e9d51fc50c2d2c63e0d2fb25; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_459e9d51fc50c2d2c63e0d2fb25" FOREIGN KEY ("statusEffectId") REFERENCES public.status_effect(id) ON DELETE CASCADE;


--
-- Name: race FK_509e14d0693cd13e63114746334; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.race
    ADD CONSTRAINT "FK_509e14d0693cd13e63114746334" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: skill FK_5728d17b7708e3ac07a9c1382c7; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill
    ADD CONSTRAINT "FK_5728d17b7708e3ac07a9c1382c7" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: status_effect_skill_modifiers_skill_modifier FK_5be50d8fcc7a8ae57732aabd93f; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_5be50d8fcc7a8ae57732aabd93f" FOREIGN KEY ("skillModifierId") REFERENCES public.skill_modifier(id) ON DELETE CASCADE;


--
-- Name: equipment_stat_modifiers_stat_modifier FK_5d6dbff6be99f1f751ddd25bdb9; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_5d6dbff6be99f1f751ddd25bdb9" FOREIGN KEY ("equipmentId") REFERENCES public.equipment(id) ON DELETE CASCADE;


--
-- Name: character_effect FK_6598eb68c73c94cf2928f28d1fc; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect
    ADD CONSTRAINT "FK_6598eb68c73c94cf2928f28d1fc" FOREIGN KEY ("statusEffectId") REFERENCES public.status_effect(id);


--
-- Name: character_effect FK_6f8d32646cd388c667b27e5135d; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_effect
    ADD CONSTRAINT "FK_6f8d32646cd388c667b27e5135d" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character FK_74a400a1643ce47965a8a231bbf; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_74a400a1643ce47965a8a231bbf" FOREIGN KEY ("clazzId") REFERENCES public.class(id);


--
-- Name: item FK_76ba3e80c7d03c62d171ae152ea; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.item
    ADD CONSTRAINT "FK_76ba3e80c7d03c62d171ae152ea" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: status_effect FK_774ac5e5c5f2ab7d71e50b60aea; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect
    ADD CONSTRAINT "FK_774ac5e5c5f2ab7d71e50b60aea" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: character FK_7b670580b08c1e3b25fc3658c90; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_7b670580b08c1e3b25fc3658c90" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: status_effect FK_906ceaa00830b7e540b68bc7d1d; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect
    ADD CONSTRAINT "FK_906ceaa00830b7e540b68bc7d1d" FOREIGN KEY ("universalModifierId") REFERENCES public.universal_modifier(id);


--
-- Name: equipment_skill_modifiers_skill_modifier FK_90b8d41223816f64c0d1e23c1f1; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_90b8d41223816f64c0d1e23c1f1" FOREIGN KEY ("skillModifierId") REFERENCES public.skill_modifier(id) ON DELETE CASCADE;


--
-- Name: character_stat FK_92bc9e28e14d2d03292e0d28a94; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_stat
    ADD CONSTRAINT "FK_92bc9e28e14d2d03292e0d28a94" FOREIGN KEY ("statId") REFERENCES public.stat(id);


--
-- Name: character_item FK_a533a7339e040463d5d2bfa7da6; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item
    ADD CONSTRAINT "FK_a533a7339e040463d5d2bfa7da6" FOREIGN KEY ("characterId") REFERENCES public."character"(id);


--
-- Name: character FK_a7485d062b19695002b6175e8fb; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT "FK_a7485d062b19695002b6175e8fb" FOREIGN KEY ("raceId") REFERENCES public.race(id);


--
-- Name: character_item FK_a9ccbb5fe58c0fcc9be39bda7ea; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_item
    ADD CONSTRAINT "FK_a9ccbb5fe58c0fcc9be39bda7ea" FOREIGN KEY ("itemId") REFERENCES public.item(id);


--
-- Name: status_effect_stat_modifiers_stat_modifier FK_b21f62444df75b2bf76d0eeea61; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.status_effect_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_b21f62444df75b2bf76d0eeea61" FOREIGN KEY ("statModifierId") REFERENCES public.stat_modifier(id) ON DELETE CASCADE;


--
-- Name: equipment_stat_modifiers_stat_modifier FK_bb9c6775cea83d5c0decb498482; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_stat_modifiers_stat_modifier
    ADD CONSTRAINT "FK_bb9c6775cea83d5c0decb498482" FOREIGN KEY ("statModifierId") REFERENCES public.stat_modifier(id) ON DELETE CASCADE;


--
-- Name: stat_modifier FK_d001450c9d60eaf9fc04fa53a1a; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat_modifier
    ADD CONSTRAINT "FK_d001450c9d60eaf9fc04fa53a1a" FOREIGN KEY ("statId") REFERENCES public.stat(id);


--
-- Name: character_skill FK_e12377dbce6154a7bcb2db70cbc; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.character_skill
    ADD CONSTRAINT "FK_e12377dbce6154a7bcb2db70cbc" FOREIGN KEY ("skillId") REFERENCES public.skill(id);


--
-- Name: skill_modifier FK_e559875de56112fb6ecd75b2fe9; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.skill_modifier
    ADD CONSTRAINT "FK_e559875de56112fb6ecd75b2fe9" FOREIGN KEY ("skillId") REFERENCES public.skill(id);


--
-- Name: equipment_skill_modifiers_skill_modifier FK_e73e4d8a0f2ae9a1816968fdc43; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment_skill_modifiers_skill_modifier
    ADD CONSTRAINT "FK_e73e4d8a0f2ae9a1816968fdc43" FOREIGN KEY ("equipmentId") REFERENCES public.equipment(id) ON DELETE CASCADE;


--
-- Name: stat FK_f3c5b7a4bb65961c54dddcad16a; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.stat
    ADD CONSTRAINT "FK_f3c5b7a4bb65961c54dddcad16a" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- Name: equipment FK_f87c9fb7bed46932a98eeeb0c67; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT "FK_f87c9fb7bed46932a98eeeb0c67" FOREIGN KEY ("universalModifierId") REFERENCES public.universal_modifier(id);


--
-- Name: equipment FK_fb5b513ae2040f78911353ae9a6; Type: FK CONSTRAINT; Schema: public; Owner: typeormtest
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT "FK_fb5b513ae2040f78911353ae9a6" FOREIGN KEY ("settingId") REFERENCES public.setting(id);


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

